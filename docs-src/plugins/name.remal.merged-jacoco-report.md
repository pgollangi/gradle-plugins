The plugin applies [`name.remal.common-settings`](name.remal.common-settings.md) plugin.

&nbsp;

This plugin merges all [`jacoco`](https://docs.gradle.org/current/userguide/jacoco_plugin.html) plugin reports and creates `displayMergedJacocoReport` task that displays this message to console: `Merged code coverage: 50.00% (500 of 1000 lines)`.
