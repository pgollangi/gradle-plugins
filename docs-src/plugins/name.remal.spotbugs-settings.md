**This plugin works only if [`java`](https://docs.gradle.org/current/userguide/java_plugin.html) and [`com.github.spotbugs`](https://plugins.gradle.org/plugin/com.github.spotbugs) plugins are applied.**

&nbsp;

This plugin helps to configure SpotBugs.

* It updates SpotBugs version to the latest one.
* It forces SpotBugs dependencies to have the same version as SpotBugs itself.
* It creates a draft of SpotBugs exclusions file in `$rootDir/gradle/spotbugs/exclude.xml`. This file will will be used as default value of `spotbugs.excludeFilter`.
* It excludes all [relocated classes](name.remal.classes-relocation.md) from the analysis.
* It enables XML report, customize HTML report and print all violations to console.

Also the plugin creates `spotbugs.excludes` extension of type [`ExcludesExtension`](#nameremalgradle_pluginspluginscode_qualityexcludesextension) to setup default exclusions. The same extension is created for every SpotBugs task to extend exclusions.

&nbsp;

### `name.remal.gradle_plugins.plugins.code_quality.ExcludesExtension`
| Property | Type | Description
| --- | :---: | --- |
| `classNames` | <nobr>`MutableSet<String>`<nobr> | ANT like exclude patterns of class names. |
| `sources` | <nobr>`MutableSet<String>`<nobr> | ANT like exclude patterns of source files. |
| `messages` | <nobr>`MutableSet<String>`<nobr> | Violation messages to exclude. |

| Method | Description
| --- | --- |
| `void className(String value)` | Add ANT like exclude pattern of class names. |
| `void classNames(String... values)` | Add ANT like exclude patterns of class names. |
| `void classNames(Iterable<String> values)` | Add ANT like exclude patterns of class names. |
| `void source(String value)` | Add ANT like exclude pattern of source files. |
| `void sources(String... values)` | Add ANT like exclude patterns of source files. |
| `void sources(Iterable<String> values)` | Add ANT like exclude patterns of source files. |
| `void message(String value)` | Add violation message to exclude. |
| `void messages(String... values)` | Add violation messages to exclude. |
| `void messages(Iterable<String> values)` | Add violation messages to exclude. |
| `void kotlin()` | Exclude Kotlin sources. |
| `void groovy()` | Exclude Groovy sources. |
