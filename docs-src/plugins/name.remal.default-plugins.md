The plugin applies these plugins:

* [`name.remal.common-settings`](name.remal.common-settings.md)
* [`name.remal.root-project-default-plugins`](name.remal.root-project-default-plugins.md)
* [`name.remal.buildSrc-default-plugins`](name.remal.buildSrc-default-plugins.md)
* [`name.remal.dependencies-modifier`](name.remal.dependencies-modifier.md)
* [`name.remal.default-dependency-version`](name.remal.default-dependency-version.md)
* [`name.remal.transitive-dependencies`](name.remal.transitive-dependencies.md)
* [`name.remal.dependency-versions`](name.remal.dependency-versions.md)
* [`name.remal.component-capabilities`](name.remal.component-capabilities.md)
* [`name.remal.component-metadata`](name.remal.component-metadata.md)
* [`name.remal.filtering-settings`](name.remal.filtering-settings.md)<br><br>
* [`name.remal.java-settings`](name.remal.java-settings.md)<br><br>
* [`name.remal.kotlin-settings`](name.remal.kotlin-settings.md)
* [`name.remal.kotlin-java8-default-methods`](name.remal.kotlin-java8-default-methods.md)
* [`name.remal.kotlin-js-settings`](name.remal.kotlin-js-settings.md)<br><br>
* [`name.remal.groovy-settings`](name.remal.groovy-settings.md)<br><br>
* [`name.remal.classes-processing`](name.remal.classes-processing.md)
* [`name.remal.test-settings`](name.remal.test-settings.md)
* [`name.remal.randomize-test-classes`](name.remal.randomize-test-classes.md)
* [`name.remal.sonarqube-settings`](name.remal.sonarqube-settings.md)
* [`name.remal.signing-settings`](name.remal.signing-settings.md)
* [`name.remal.maven-publish-settings`](name.remal.maven-publish-settings.md)
* [`name.remal.maven-publish-bintray`](name.remal.maven-publish-bintray.md)
* [`name.remal.maven-publish-ossrh`](name.remal.maven-publish-ossrh.md)
* [`name.remal.maven-publish-gradle-plugin-portal`](name.remal.maven-publish-gradle-plugin-portal.md)<br><br>
* [`name.remal.all-ci`](name.remal.all-ci.md)<br><br>
