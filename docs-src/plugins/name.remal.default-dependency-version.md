This plugin sets default versions for each dependency with empty version if the dependency matches notations of [`DefaultDependencyVersion`](#nameremalgradle_pluginspluginsdependenciesdefaultdependencyversion) services.

Also `defaultDependencyVersionsHelp` task is created to display all default dependency versions for the project.

&nbsp;

The services and factories are loaded using [ServiceLoader mechanism](https://docs.oracle.com/javase/8/docs/api/java/util/ServiceLoader.html).

&nbsp;

### `name.remal.gradle_plugins.plugins.dependencies.DefaultDependencyVersion`
| Property | Type | Description
| --- | :---: | --- |
| <a id="default-dependency-version-notation"></a>`notation` | `File` | Dependency notation. |
| `version` | <nobr>`List<File>`</nobr> | Default version. |

| Method | Description
| --- | --- |
| `boolean matches(String dependencyNotation)` | Checks if `dependencyNotation` matches [`notation`](#default-dependency-version-notation). |
| `boolean matches(String? group, String? name)` | Checks if dependency `group` and `name` matches [`notation`](#default-dependency-version-notation). |

### `name.remal.gradle_plugins.plugins.dependencies.DefaultDependencyVersionFactory`
| Method | Description
| --- | --- |
| <code>List&lt;<a href="#nameremalgradle_pluginspluginsdependenciesdefaultdependencyversion">DefaultDependencyVersion</a>> create(<a href="https://docs.gradle.org/current/javadoc/org/gradle/api/Project.html">Project</a> project)</code> | Create a list of `DefaultDependencyVersion` services for the project. |
