**This plugin works only if [`eclipse`](https://docs.gradle.org/current/userguide/eclipse_plugin.html) plugin is applied.**

* Apply [`eclipse`](https://docs.gradle.org/current/userguide/eclipse_plugin.html) and this plugins for all projects
* It turns ON downloading dependency javadoc artifacts.
* It turns ON downloading dependency sources artifacts.
