**This plugin works only for [`buildSrc`](https://docs.gradle.org/current/userguide/organizing_gradle_projects.html#sec:build_sources) project.**

The plugin applies these plugins:

* [`java`](https://docs.gradle.org/current/userguide/java_plugin.html)
* [`jacoco`](https://docs.gradle.org/current/userguide/jacoco_plugin.html)
* [`name.remal.default-plugins`](name.remal.default-plugins.md)
* [`name.remal.merged-jacoco-report`](name.remal.merged-jacoco-report.md)
