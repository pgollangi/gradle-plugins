**This plugin works only if [`java`](https://docs.gradle.org/current/userguide/java_plugin.html) plugin is applied.**

The plugin applies these plugins:

* [`name.remal.common-settings`](name.remal.common-settings.md)
* [`name.remal.transitive-dependencies`](name.remal.transitive-dependencies.md)
* [`name.remal.apt`](name.remal.apt.md)
* [`name.remal.classes-processing`](name.remal.classes-processing.md)
* [`name.remal.merge-resources`](name.remal.merge-resources.md)
* [`name.remal.test-settings`](name.remal.test-settings.md)
* [`name.remal.test-source-sets`](name.remal.test-source-sets.md)
* [`name.remal.checkstyle-settings`](name.remal.checkstyle-settings.md)
* [`name.remal.jacoco-settings`](name.remal.jacoco-settings.md)
* [`name.remal.findbugs-settings`](name.remal.findbugs-settings.md)
* [`name.remal.spotbugs-settings`](name.remal.spotbugs-settings.md)
* [`name.remal.java-application-settings`](name.remal.java-application-settings.md)

&nbsp;

This plugin helps to configure [`java`](https://docs.gradle.org/current/userguide/java_plugin.html) plugin.

* Creates `compileOnlyAll` configuration. All `compileOnly` configurations extend it.
* Creates `compileOptional` configuration. `compileOnly` configuration extends it. Also `compile` configurations of all source-sets **except main** extend it. Dependencies from `compileOptional` configuration are added to all [`Test`](https://docs.gradle.org/current/javadoc/org/gradle/api/tasks/testing/Test.html) and [`JavaExec`](https://docs.gradle.org/current/javadoc/org/gradle/api/tasks/JavaExec.html) tasks including transitive dependencies. Transitive dependencies can be configured using [name.remal.transitive-dependencies](name.remal.transitive-dependencies.md) plugin.
* Uses `mavenCentral` and `mavenLocal` repositories by default.
* Sets sources default encoding to `UTF-8`.
* Enables displaying deprecation warnings.
* Adds `-parameters` compiler option if targetting Java 8 and above.
* If sources are compatible with Java 9 and above:
    * Adds `--module-path` compiler option. It equals to classpath plus annotation-processor path.
    * Adds `--patch-module` compiler option. It allows to have multiple output classes dirs, for example Java and Kotlin.
* Adds `Automatic-Module-Name` manifest attribute in result JAR archive if targetting Java 8 or below.
