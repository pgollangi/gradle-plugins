The plugin applies these plugins:

* [`name.remal.dependencies-filter`](name.remal.dependencies-filter.md)

&nbsp;

1. Adds `dependencies.toolsJar` extension method. This method returns a dependency of Java `tools.jar`.
1. <a id="embedded-kotlin"></a> Adds `dependencies.embeddedKotlin` extension method. This method returns a dependency of embedded Kotlin libraries.
1. <a id="corresponding-kotlin"></a> Adds `dependencies.correspondingKotlin` extension method. This method returns a Kotlin dependency corresponding to embedded Kotlin.
1. <a id="corresponding-kotlin-gradle-plugin"></a> Adds `dependencies.correspondingKotlinGradlePlugin` extension method. This method returns a dependency of `kotlin` plugin the same version as embedded Kotlin.
