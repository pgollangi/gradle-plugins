**This plugin works only if [`kotlin`](https://kotlinlang.org/docs/reference/using-gradle.html) plugin is applied.**

The plugin applies [`name.remal.java-settings`](name.remal.java-settings.md) plugin.

The plugin applies these plugins if they are available:

* `kotlin-allopen`
* `kotlin-jpa`
* `kotlin-noarg`
* `kotlin-spring`

&nbsp;

This plugin helps to configure [`kotlin`](https://kotlinlang.org/docs/reference/using-gradle.html) plugin.

* If `targetCompatibility` is Java 8 compatible:
    * Sets `jvmTarget`.
    * Sets `javaParameters` to `true`.
    * Adds `-Xjvm-default=compatibility` compiler flag.
* Sets `kapt.correctErrorTypes` to `true` if `kapt` Kotlin plugin enabled.
* If `all-open` Kotlin plugin is applied, adds these annotations as open-annotations:
    * `javax.inject.Inject`
