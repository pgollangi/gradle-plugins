**This plugin works only if `CI` and `TRAVIS` environment variables equals to `true`.**

The plugin applies [`name.remal.common-ci`](name.remal.common-ci.md) plugin.

Current build is treated as executed on a CI server if `CI` and `TRAVIS` environment variables equals to `true`.

&nbsp;

The plugin sets [`ci`](name.remal.common-ci.md#nameremalgradle_pluginspluginsciciextension) extension values:

* `pipelineId` to `null`
* `buildId` to `TRAVIS_BUILD_NUMBER` environment variable (`TRAVIS_BUILD_ID` as fallback)
* `stageName` to `TRAVIS_BUILD_STAGE_NAME` environment variable
* `jobName` to `TRAVIS_JOB_NAME` environment variable (`TRAVIS_JOB_NUMBER` and then `TRAVIS_JOB_ID` as fallbacks)

&nbsp;

## If [`name.remal.vcs-operations`](name.remal.vcs-operations.md) plugin is applied, this plugin:

* Sets `vcsOperations.overwriteCurrentBranch` to `TRAVIS_PULL_REQUEST_BRANCH` or `TRAVIS_BRANCH` environment variable (only if `TRAVIS_TAG` environment variable is empty)
