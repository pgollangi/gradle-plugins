**Applying this plugin makes sense if you're developing a Gradle plugins**

The plugin applies these plugins:

* [`java`](https://docs.gradle.org/current/userguide/java_plugin.html)
* [`name.remal.common-settings`](name.remal.common-settings.md)
* [`name.remal.dependency-versions`](name.remal.dependency-versions.md)

&nbsp;

The main plugin's functionality is creating clones for [`Test`](https://docs.gradle.org/current/javadoc/org/gradle/api/tasks/testing/Test.html) tasks. Cloned tasks have replaced dependencies:

* `localGroovy` to `name.remal.gradle-api:local-groovy`
* `gradleApi` to `name.remal.gradle-api:gradle-api`
* `gradleTestKit` to `name.remal.gradle-api:gradle-test-kit`
* [`embeddedKotlin`](name.remal.dependencies-extensions.md#embedded-kotlin) to `name.remal.gradle-api:embedded-kotlin`
* [`correspondingKotlin`](name.remal.dependencies-extensions.md#corresponding-kotlin) to `name.remal.gradle-api:embedded-kotlin`
* [`correspondingKotlinGradlePlugin`](name.remal.dependencies-extensions.md#corresponding-kotlin-gradle-plugin) to `name.remal.gradle-api:corresponding-kotlin-plugin`

For each Gradle version a `Test` task clone is created.
