This plugin configures SonarQube properties:
* Make [`testSourceSets`](name.remal.test-source-sets.md) recognized as test sources
* All sources of projects with [`name.remal.test-fixtures`](name.remal.test-fixtures.md) plugin applied configured as test sources
    * All files are excluded from coverage reports for such projects
