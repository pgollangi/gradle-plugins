package name.remal.gradle_plugins.testing.dsl

import org.gradle.api.Project
import org.gradle.api.internal.project.ProjectInternal

fun Project.evaluate() {
    this as ProjectInternal
    evaluate()
}
