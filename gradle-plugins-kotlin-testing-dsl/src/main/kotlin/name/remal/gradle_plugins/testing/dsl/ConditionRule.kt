package name.remal.gradle_plugins.testing.dsl

import org.junit.rules.TestRule
import org.junit.runner.Description
import org.junit.runners.model.Statement

class ConditionRule(private val condition: () -> Boolean) : TestRule {

    override fun apply(base: Statement, description: Description): Statement {
        if (condition()) {
            return base
        } else {
            return object : Statement() {
                override fun evaluate() {
                    // do nothing
                }
            }
        }
    }

}
