[![Maven Central](https://maven-badges.herokuapp.com/maven-central/name.remal/gradle-plugins/badge.svg)](https://maven-badges.herokuapp.com/maven-central/name.remal/gradle-plugins)

**The plugins' documentation is located here: https://remal.gitlab.io/gradle-plugins**
