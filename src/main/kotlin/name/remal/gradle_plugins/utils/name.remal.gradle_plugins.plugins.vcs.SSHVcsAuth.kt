package name.remal.gradle_plugins.utils

import com.jcraft.jsch.JSch
import com.jcraft.jsch.Session
import name.remal.gradle_plugins.dsl.DEFAULT_IO_TIMEOUT
import name.remal.gradle_plugins.plugins.vcs.SSHVcsAuth
import org.eclipse.jgit.transport.JschConfigSessionFactory
import org.eclipse.jgit.transport.OpenSshConfig
import org.eclipse.jgit.transport.SshSessionFactory
import org.eclipse.jgit.util.FS
import java.nio.CharBuffer
import java.nio.charset.StandardCharsets.UTF_8

fun SSHVcsAuth.toSshSessionFactory(): SshSessionFactory = object : JschConfigSessionFactory() {

    override fun configure(host: OpenSshConfig.Host, session: Session) {
        session.setConfig("StrictHostKeyChecking", "no")
        session.setConfig("ConnectTimeout", DEFAULT_IO_TIMEOUT.seconds.toString())
    }

    private val jSch: JSch by lazy {
        JSch().apply {
            configureJSch(this)
            addIdentity(
                privateKeyFile.absolutePath,
                password?.let {
                    val charBuffer = CharBuffer.wrap(it)
                    try {
                        val byteBuffer = UTF_8.encode(charBuffer)
                        try {
                            return@let byteBuffer.array().copyOfRange(byteBuffer.position(), byteBuffer.limit())
                        } finally {
                            byteBuffer.array().fill(0)
                        }
                    } finally {
                        charBuffer.array().fill('\u0000')
                    }
                }
            )
        }
    }

    override fun getJSch(host: OpenSshConfig.Host, fs: FS) = jSch

}
