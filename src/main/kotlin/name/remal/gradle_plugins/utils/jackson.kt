package name.remal.gradle_plugins.utils

import name.remal.json.data_format.DataFormatJSON

internal val JSON_OBJECT_MAPPER = DataFormatJSON.JSON_DATA_FORMAT.objectMapper
