package name.remal.gradle_plugins.utils

import okhttp3.HttpUrl
import retrofit2.Retrofit
import java.net.URI

fun Retrofit.Builder.baseUrl(uri: URI) = baseUrl(HttpUrl.get(uri) ?: throw IllegalArgumentException("Not a valid HTTP URI: $uri"))

fun <T> Retrofit.Builder.create(service: Class<T>): T {
    return build().create(service)
}
