package name.remal.gradle_plugins.plugins.testing

import org.gradle.api.tasks.testing.Test

interface TestFrameworkConfigurer {

    fun configure(task: Test)

}
