package name.remal.gradle_plugins.plugins.testing

import name.remal.OrderedHigher
import name.remal.gradle_plugins.api.AutoService
import name.remal.gradle_plugins.dsl.GradleEnumVersion.GRADLE_VERSION_4_6
import name.remal.gradle_plugins.dsl.artifact.CachedArtifactsCollection
import name.remal.gradle_plugins.dsl.extensions.compareTo
import name.remal.gradle_plugins.dsl.extensions.doSetup
import name.remal.gradle_plugins.dsl.extensions.forTemp
import name.remal.gradle_plugins.dsl.extensions.hasDependency
import name.remal.gradle_plugins.dsl.extensions.logWarn
import name.remal.gradle_plugins.dsl.extensions.toHasEntries
import name.remal.gradle_plugins.utils.getVersionProperty
import org.gradle.api.artifacts.Configuration
import org.gradle.api.file.FileCollection
import org.gradle.api.tasks.testing.Test
import org.gradle.util.GradleVersion

object TestFrameworkConfigurerJUnitPlatform : TestFrameworkConfigurer {

    override fun configure(task: Test) {
        task.useJUnitPlatform {
        }

        task.doSetup {
            val classpath = task.classpath
            if (classpath.toHasEntries().classNames.any { it.startsWith("org.junit.jupiter.engine.") }) return@doSetup

            task.project.configurations.forTemp("jupiterEngine") { conf ->
                val version = task.project.getVersionProperty("junit-jupiter-engine")
                conf.dependencies.add(task.project.dependencies.create("org.junit.jupiter:junit-jupiter-engine:$version"))
                if (conf.resolvedConfiguration.hasError()) {
                    try {
                        conf.resolvedConfiguration.rethrowFailure()
                    } catch (e: Exception) {
                        task.logWarn(e)
                        return@forTemp
                    }
                }
                task.classpath += conf
            }
        }
    }

}


@AutoService(TestFrameworkConfigurerDetector::class)
class TestFrameworkConfigurerDetectorJUnitPlatformDependency : TestFrameworkConfigurerDetector, OrderedHigher<TestFrameworkConfigurerDetector> {

    override fun detect(classpath: FileCollection): TestFrameworkConfigurer? {
        if (GradleVersion.current() < GRADLE_VERSION_4_6) return null
        if (classpath !is Configuration) return null
        if (classpath.hasDependency("org.junit.jupiter", "*")) return TestFrameworkConfigurerJUnitPlatform
        if (classpath.hasDependency("org.junit.vintage", "*")) return TestFrameworkConfigurerJUnitPlatform
        return null
    }

}

@AutoService
class TestFrameworkConfigurerDetectorJUnitPlatformClasspath : TestFrameworkConfigurerDetector {

    override fun detect(classpath: FileCollection): TestFrameworkConfigurer? {
        if (GradleVersion.current() < GRADLE_VERSION_4_6) return null
        if (CachedArtifactsCollection(classpath).containsClass("org.junit.jupiter.api.Test")) return TestFrameworkConfigurerJUnitPlatform
        return null
    }

}
