package name.remal.gradle_plugins.plugins.testing

import org.gradle.api.tasks.SourceSetContainer

interface TestSourceSetContainer : SourceSetContainer
