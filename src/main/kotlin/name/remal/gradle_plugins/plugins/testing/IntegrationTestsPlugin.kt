package name.remal.gradle_plugins.plugins.testing

import name.remal.gradle_plugins.dsl.ApplyPluginClasses
import name.remal.gradle_plugins.dsl.BaseReflectiveProjectPlugin
import name.remal.gradle_plugins.dsl.Plugin
import name.remal.gradle_plugins.dsl.PluginAction
import name.remal.gradle_plugins.dsl.WithPlugins
import name.remal.gradle_plugins.plugins.java.JavaPluginId

@Plugin(
    id = "name.remal.integration-tests",
    description = "Plugin that adds 'integration' source set and configures it.",
    tags = ["java", "test", "integration"]
)
@WithPlugins(JavaPluginId::class)
@ApplyPluginClasses(TestSourceSetsPlugin::class)
class IntegrationTestsPlugin : BaseReflectiveProjectPlugin() {

    @PluginAction(isHidden = true)
    fun TestSourceSetContainer.`Add 'integration' source set`() {
        create("integration")
    }

}
