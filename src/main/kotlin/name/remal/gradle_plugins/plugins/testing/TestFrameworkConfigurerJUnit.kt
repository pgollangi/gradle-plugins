package name.remal.gradle_plugins.plugins.testing

import name.remal.OrderedHigher
import name.remal.gradle_plugins.api.AutoService
import name.remal.gradle_plugins.dsl.artifact.CachedArtifactsCollection
import name.remal.gradle_plugins.dsl.extensions.hasDependency
import org.gradle.api.artifacts.Configuration
import org.gradle.api.file.FileCollection
import org.gradle.api.tasks.testing.Test

object TestFrameworkConfigurerJUnit : TestFrameworkConfigurer {
    override fun configure(task: Test) {
        task.useJUnit {
        }
    }
}


@AutoService(TestFrameworkConfigurerDetector::class)
class TestFrameworkConfigurerDetectorJUnitDependency : TestFrameworkConfigurerDetector, OrderedHigher<TestFrameworkConfigurerDetector> {

    override fun detect(classpath: FileCollection): TestFrameworkConfigurer? {
        if (classpath !is Configuration) return null
        if (classpath.hasDependency("junit", "junit")) return TestFrameworkConfigurerJUnit
        return null
    }

    override fun getOrder(): Int = Int.MAX_VALUE

}

@AutoService
class TestFrameworkConfigurerDetectorJUnitClasspath : TestFrameworkConfigurerDetector {

    override fun detect(classpath: FileCollection): TestFrameworkConfigurer? {
        if (CachedArtifactsCollection(classpath).containsClass("org.junit.Test")) return TestFrameworkConfigurerJUnit
        return null
    }

    override fun getOrder(): Int = Int.MAX_VALUE

}
