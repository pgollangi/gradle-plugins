package name.remal.gradle_plugins.plugins.classes_processing

import name.remal.Ordered

interface ClassesProcessingFilter : Ordered<ClassesProcessingFilter> {
    fun canBytecodeBeProcessed(bytecode: ByteArray): Boolean
}
