package name.remal.gradle_plugins.plugins.classes_processing.processors.todo

import name.remal.gradle_plugins.api.AutoService
import name.remal.gradle_plugins.api.classes_processing.ClassesProcessor
import name.remal.gradle_plugins.api.classes_processing.ClassesProcessorsGradleTaskFactory
import name.remal.gradle_plugins.api.todo.FIXME
import name.remal.gradle_plugins.dsl.extensions.toHasEntries
import org.gradle.api.tasks.compile.AbstractCompile

@AutoService
class ToDoClassesProcessorFactory : ClassesProcessorsGradleTaskFactory {

    override fun createClassesProcessors(compileTask: AbstractCompile): List<ClassesProcessor> {
        if (!compileTask.classpath.toHasEntries().containsClass(name.remal.gradle_plugins.api.todo.TODO::class.java)
            && !compileTask.classpath.toHasEntries().containsClass(FIXME::class.java)
        ) {
            return emptyList()
        }
        return listOf(ToDoClassesProcessor())
    }

}
