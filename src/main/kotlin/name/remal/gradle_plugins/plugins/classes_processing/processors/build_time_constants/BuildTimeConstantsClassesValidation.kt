package name.remal.gradle_plugins.plugins.classes_processing.processors.build_time_constants

import name.remal.accept
import name.remal.gradle_plugins.api.BuildTimeConstants
import name.remal.gradle_plugins.api.classes_processing.BytecodeModifier
import name.remal.gradle_plugins.api.classes_processing.ClassesProcessor
import name.remal.gradle_plugins.api.classes_processing.ClassesProcessor.VALIDATION_STAGE
import name.remal.gradle_plugins.api.classes_processing.ProcessContext
import name.remal.gradle_plugins.plugins.classes_processing.ClassProcessingValidatingException
import name.remal.packageName
import org.objectweb.asm.ClassReader
import org.objectweb.asm.ClassWriter
import org.objectweb.asm.Type.getInternalName
import org.objectweb.asm.commons.ClassRemapper
import org.objectweb.asm.commons.Remapper

class BuildTimeConstantsClassesValidation : ClassesProcessor {

    companion object {
        private val forbiddenInternalClassNames: Set<String?> = setOf(
            getInternalName(BuildTimeConstants::class.java)
        )
        private val skipPackages = setOf(
            BuildTimeConstantsClassesProcessor::class.java.packageName,
            BuildTimeConstantsClassesValidation::class.java.packageName
        ).toList()
    }

    override fun process(bytecode: ByteArray, bytecodeModifier: BytecodeModifier, className: String, resourceName: String, context: ProcessContext) {
        if (skipPackages.any { className.startsWith("$it.") }) return

        val classReader = ClassReader(bytecode)
        val classWriter = ClassWriter(classReader, 0)
        val errorMessages = mutableSetOf<String>()
        classReader.accept(ClassRemapper(classWriter, object : Remapper() {
            override fun map(typeName: String?): String? {
                if (typeName in forbiddenInternalClassNames) {
                    errorMessages.add("$className uses $typeName directly")
                }
                return typeName
            }
        }))

        if (errorMessages.isNotEmpty()) {
            if (1 == errorMessages.size) {
                throw ClassProcessingValidatingException(errorMessages.first())
            } else {
                throw ClassProcessingValidatingException().apply {
                    errorMessages.forEach { addSuppressed(ClassProcessingValidatingException(it)) }
                }
            }
        }
    }

    override fun getStage() = VALIDATION_STAGE

}
