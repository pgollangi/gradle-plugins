package name.remal.gradle_plugins.plugins.fatjar

import name.remal.gradle_plugins.dsl.ApplyPluginClasses
import name.remal.gradle_plugins.dsl.BaseReflectiveProjectPlugin
import name.remal.gradle_plugins.dsl.CreateExtensionsPluginAction
import name.remal.gradle_plugins.dsl.Plugin
import name.remal.gradle_plugins.dsl.PluginAction
import name.remal.gradle_plugins.dsl.WithPlugins
import name.remal.gradle_plugins.dsl.extensions.all
import name.remal.gradle_plugins.dsl.extensions.autoFileTree
import name.remal.gradle_plugins.dsl.extensions.compileOnly
import name.remal.gradle_plugins.dsl.extensions.doSetup
import name.remal.gradle_plugins.dsl.extensions.exclude
import name.remal.gradle_plugins.dsl.extensions.get
import name.remal.gradle_plugins.plugins.common.CommonSettingsPlugin
import name.remal.gradle_plugins.plugins.java.JavaPluginId
import name.remal.gradle_plugins.plugins.merge_resources.MergeResourcesPlugin
import org.gradle.api.Project
import org.gradle.api.artifacts.Configuration
import org.gradle.api.artifacts.ConfigurationContainer
import org.gradle.api.file.DuplicatesStrategy
import org.gradle.api.plugins.ExtensionContainer
import org.gradle.api.plugins.JavaPlugin.JAR_TASK_NAME
import org.gradle.api.plugins.JavaPluginConvention
import org.gradle.api.tasks.SourceSet.MAIN_SOURCE_SET_NAME
import org.gradle.api.tasks.SourceSetContainer
import org.gradle.api.tasks.TaskContainer
import org.gradle.jvm.tasks.Jar

const val FAT_JAR_CONFIGURATION_NAME = "fatJar"

@Plugin(
    id = "name.remal.fat-jar",
    description = "Plugin that creates 'fatJar' configuration.",
    tags = ["fatjar", "fat-jar"]
)
@WithPlugins(JavaPluginId::class)
@ApplyPluginClasses(CommonSettingsPlugin::class, MergeResourcesPlugin::class)
class FatJarPlugin : BaseReflectiveProjectPlugin() {

    @CreateExtensionsPluginAction
    fun ExtensionContainer.`Create 'fatJarSettings' extension`(): FatJarSettingsExtension {
        return create("fatJarSettings", FatJarSettingsExtension::class.java)
    }

    @PluginAction
    fun ConfigurationContainer.`Create 'fatJar' configuration`(sourceSets: SourceSetContainer) {
        create(FAT_JAR_CONFIGURATION_NAME) { conf ->
            compileOnly.extendsFrom(conf)
            sourceSets.matching { MAIN_SOURCE_SET_NAME != it.name }.all {
                findByName(it.implementationConfigurationName)?.extendsFrom(conf)
            }
        }
    }

    @PluginAction
    fun TaskContainer.`Setup 'jar' task`(configurations: ConfigurationContainer, project: Project, extensions: ExtensionContainer, java: JavaPluginConvention) {
        all(Jar::class.java, JAR_TASK_NAME) {
            val fatJarConf = configurations.fatJar
            it.dependsOn(fatJarConf)
            it.doSetup { jarTask ->
                val fatJarSettings = extensions[FatJarSettingsExtension::class.java]
                fatJarConf.files.forEach { file ->
                    var fileTree = project.autoFileTree(file)
                    if (!java.sourceCompatibility.isJava9Compatible) {
                        fileTree = fileTree.exclude("module-info.class")
                    }
                    fileTree = fileTree.matching { filter ->
                        filter.exclude("META-INF/*.MF")
                        filter.exclude("META-INF/*.SF")
                        filter.exclude("META-INF/*.DSA")
                        filter.exclude("META-INF/*.RSA")

                        filter.exclude(*fatJarSettings.excludePatterns.toTypedArray())
                        fatJarSettings.excludeSpecs.forEach { filter.exclude(it) }

                        filter.exclude {
                            val path = it.path
                            if (path.endsWith(".class")) {
                                return@exclude null != ClassLoader.getSystemResource(path)
                            } else {
                                return@exclude false
                            }
                        }
                    }
                    jarTask.from(fileTree) {
                        it.duplicatesStrategy = DuplicatesStrategy.INCLUDE
                    }
                }
            }
        }
    }

}


val ConfigurationContainer.fatJar: Configuration get() = this[FAT_JAR_CONFIGURATION_NAME]
