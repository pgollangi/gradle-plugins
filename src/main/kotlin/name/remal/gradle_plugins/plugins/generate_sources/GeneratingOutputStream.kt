package name.remal.gradle_plugins.plugins.generate_sources

import java.io.BufferedOutputStream
import java.io.File
import java.io.OutputStream

open class GeneratingOutputStream(
    override val targetFile: File,
    override val relativePath: String,
    override val generateTask: BaseGenerateTask,
    delegate: OutputStream
) : BufferedOutputStream(delegate), GeneratingOutput
