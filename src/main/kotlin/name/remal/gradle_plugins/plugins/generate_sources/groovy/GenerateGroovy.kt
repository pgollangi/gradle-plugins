package name.remal.gradle_plugins.plugins.generate_sources.groovy

import name.remal.gradle_plugins.dsl.BuildTask
import name.remal.gradle_plugins.plugins.generate_sources.BaseGenerateClassTask
import org.gradle.api.tasks.CacheableTask
import java.nio.charset.Charset

@BuildTask
@CacheableTask
class GenerateGroovy : BaseGenerateClassTask<GeneratingGroovyClassWriter>() {

    override fun classFile(packageName: String, simpleName: String, action: (writer: GeneratingGroovyClassWriter) -> Unit) {
        val relativePath = buildString {
            if (packageName.isNotEmpty()) append(packageName.replace('.', '/')).append('/')
            append(simpleName).append(".groovy")
        }
        addGenerateAction(relativePath) { file ->
            GeneratingGroovyClassWriter(packageName, simpleName, file, relativePath, this, file.writer(Charset.forName(charset))).use(action)
        }
    }

}
