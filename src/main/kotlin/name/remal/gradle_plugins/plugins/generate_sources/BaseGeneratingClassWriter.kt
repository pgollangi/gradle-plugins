package name.remal.gradle_plugins.plugins.generate_sources

import java.io.File
import java.io.StringWriter
import java.io.Writer

abstract class BaseGeneratingClassWriter<Self : BaseGeneratingClassWriter<Self>>(
    override val packageName: String,
    val simpleName: String,
    targetFile: File,
    relativePath: String,
    generateTask: BaseGenerateTask,
    delegate: Writer,
    protected val wrapDepth: Int = 0
) : GeneratingWriter(targetFile, relativePath, generateTask, delegate), GeneratingClassWriterInterface<Self> {

    protected abstract fun wrapStringWriter(stringWriter: StringWriter): Self

    override fun writeBlock(expression: String, blockAction: Self.() -> Unit) = writeBlock(this::wrapStringWriter, expression, blockAction)


    override fun toString(): String {
        if (wrapDepth == 0) {
            return super.toString()
        } else {
            return super.toString() + "(wrapDepth = $wrapDepth)"
        }
    }

}
