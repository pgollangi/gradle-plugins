package name.remal.gradle_plugins.plugins.generate_sources.groovy

import name.remal.gradle_plugins.plugins.generate_sources.BaseGenerateTask
import name.remal.gradle_plugins.plugins.generate_sources.BaseGeneratingJavaClassWriter
import java.io.File
import java.io.StringWriter
import java.io.Writer

class GeneratingGroovyClassWriter(
    packageName: String,
    simpleName: String,
    targetFile: File,
    relativePath: String,
    generateTask: BaseGenerateTask,
    delegate: Writer,
    wrapDepth: Int = 0
) : BaseGeneratingJavaClassWriter<GeneratingGroovyClassWriter>(packageName, simpleName, targetFile, relativePath, generateTask, delegate, wrapDepth),
    GeneratingGroovyClassWriterInterface<GeneratingGroovyClassWriter> {

    override fun wrapStringWriter(stringWriter: StringWriter) = GeneratingGroovyClassWriter(packageName, simpleName, targetFile, relativePath, generateTask, stringWriter, wrapDepth + 1)

}
