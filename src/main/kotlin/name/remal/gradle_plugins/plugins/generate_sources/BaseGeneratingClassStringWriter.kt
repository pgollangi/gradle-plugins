package name.remal.gradle_plugins.plugins.generate_sources

import java.io.File

abstract class BaseGeneratingClassStringWriter<Self : BaseGeneratingClassStringWriter<Self>>(
    override val packageName: String = "",
    val simpleName: String = "",
    classpath: Iterable<File> = emptyList(),
    protected val wrapDepth: Int = 0
) : GeneratingStringWriter(classpath), GeneratingClassWriterInterface<Self> {

    protected abstract val classFileExtension: String

    val relativePath = buildString {
        if (packageName.isNotEmpty()) append(packageName.replace('.', '/')).append('/')
        append(simpleName).append('.').append(classFileExtension)
    }

    protected abstract fun newSubWriter(): Self

    override fun writeBlock(expression: String, blockAction: Self.() -> Unit) = writeBlock(this::newSubWriter, expression, blockAction)

}
