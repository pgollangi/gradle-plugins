package name.remal.gradle_plugins.plugins.null_checks

import name.remal.gradle_plugins.dsl.ApplyPluginClasses
import name.remal.gradle_plugins.dsl.BaseReflectiveProjectPlugin
import name.remal.gradle_plugins.dsl.Plugin
import name.remal.gradle_plugins.dsl.WithPlugins
import name.remal.gradle_plugins.plugins.classes_processing.ClassesProcessingPlugin
import name.remal.gradle_plugins.plugins.common.CommonSettingsPlugin
import name.remal.gradle_plugins.plugins.java.JavaPluginId

@Plugin(
    id = "name.remal.insert-null-checks",
    description = "Plugin that inserts null checks into compiled class files",
    tags = ["java", "null", "nullability"]
)
@WithPlugins(JavaPluginId::class)
@ApplyPluginClasses(CommonSettingsPlugin::class, ClassesProcessingPlugin::class)
class InsertNullChecksPlugin : BaseReflectiveProjectPlugin()
