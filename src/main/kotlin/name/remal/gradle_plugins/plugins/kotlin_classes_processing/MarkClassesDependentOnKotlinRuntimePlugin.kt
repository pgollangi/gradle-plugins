package name.remal.gradle_plugins.plugins.kotlin_classes_processing

import name.remal.ASM_API
import name.remal.accept
import name.remal.buildSet
import name.remal.classNameToResourceName
import name.remal.gradle_plugins.api.AutoService
import name.remal.gradle_plugins.api.BuildTimeConstants.getClassDescriptor
import name.remal.gradle_plugins.api.classes_processing.BytecodeModifier
import name.remal.gradle_plugins.api.classes_processing.ClassesProcessor
import name.remal.gradle_plugins.api.classes_processing.ClassesProcessor.COLLECTION_STAGE
import name.remal.gradle_plugins.api.classes_processing.ClassesProcessorsGradleTaskFactory
import name.remal.gradle_plugins.api.classes_processing.ProcessContext
import name.remal.gradle_plugins.dsl.ApplyPluginClasses
import name.remal.gradle_plugins.dsl.BaseReflectiveProjectPlugin
import name.remal.gradle_plugins.dsl.Plugin
import name.remal.gradle_plugins.dsl.WithPlugins
import name.remal.gradle_plugins.dsl.extensions.isPluginApplied
import name.remal.gradle_plugins.dsl.extensions.isPluginAppliedAndNotDisabled
import name.remal.gradle_plugins.dsl.utils.ClassDependenciesCollector
import name.remal.gradle_plugins.plugins.classes_processing.ClassesProcessingPlugin
import name.remal.gradle_plugins.plugins.kotlin.KotlinJvmPluginId
import org.gradle.api.tasks.compile.AbstractCompile
import org.objectweb.asm.ClassReader
import org.objectweb.asm.ClassVisitor
import org.objectweb.asm.ClassWriter
import org.objectweb.asm.Type.getObjectType
import java.io.Closeable
import java.util.concurrent.atomic.AtomicReference

@Plugin(
    id = "name.remal.mark-classes-dependent-on-kotlin-runtime",
    description = "Process compiled classes and add RequiresKotlinRuntime annotation if class/method requires Kotlin runtime",
    tags = ["kotlin"],
    isHidden = true
)
@WithPlugins(KotlinJvmPluginId::class)
@ApplyPluginClasses(ClassesProcessingPlugin::class)
class MarkClassesDependentOnKotlinRuntimePlugin : BaseReflectiveProjectPlugin()


class MarkClassesDependentOnKotlinRuntimeClassesProcessor : ClassesProcessor, Closeable {

    companion object {
        private val requiresKotlinRuntimeDesc = getClassDescriptor(RequiresKotlinRuntime::class.java)
    }

    private val classDependenciesCollector = AtomicReference<ClassDependenciesCollector>(null)

    override fun process(bytecode: ByteArray, bytecodeModifier: BytecodeModifier, className: String, resourceName: String, context: ProcessContext) {
        val classDependenciesCollector = this.classDependenciesCollector.updateAndGet { prev ->
            if (prev != null) return@updateAndGet prev
            return@updateAndGet ClassDependenciesCollector(
                {
                    !it.startsWith("java.")
                        && !it.startsWith("javax.")
                        && !it.startsWith("groovy.")
                        && !it.startsWith("scala.")
                },
                {
                    val classResourceName = classNameToResourceName(it)
                    context.readBinaryResource(classResourceName)
                        ?: context.readClasspathBinaryResource(classResourceName)
                }
            )
        }

        val allDependenciesMap = classDependenciesCollector.getAllDependenciesMap(className)
        val kotlinDependencies = buildSet<String> {
            fun isClassDependentOnKotlin(className: String, recursive: List<String> = emptyList()): Boolean {
                if (className.startsWith("kotlin.")) return true
                val dependencies = allDependenciesMap[className] ?: return false
                return dependencies.any { it !in recursive && isClassDependentOnKotlin(it, recursive + listOf(it)) }
            }

            allDependenciesMap[className]?.filter { isClassDependentOnKotlin(it) }?.let { addAll(it) }
        }

        if (kotlinDependencies.isNotEmpty()) {
            val classReader = ClassReader(bytecode)
            val classWriter = ClassWriter(classReader, 0)

            val annotationAdder = object : ClassVisitor(ASM_API, classWriter) {
                override fun visit(version: Int, access: Int, name: String?, signature: String?, superName: String?, interfaces: Array<String>?) {
                    super.visit(version, access, name, signature, superName, interfaces)
                    super.visitAnnotation(requiresKotlinRuntimeDesc, false)
                        ?.also {
                            it.visitArray(RequiresKotlinRuntime::value.name)
                                ?.also { visitor ->
                                    kotlinDependencies.forEach {
                                        visitor.visit(null, getObjectType(it.replace('/', '.')))
                                    }
                                }
                                ?.visitEnd()
                        }
                        ?.visitEnd()
                }
            }

            classReader.accept(annotationAdder)
            bytecodeModifier.modify(classWriter.toByteArray())
        }
    }


    override fun getStage() = COLLECTION_STAGE

    override fun close() {
        classDependenciesCollector.get()?.close()
    }

}

@AutoService
class MarkClassesDependentOnKotlinRuntimeClassesProcessorFactory : ClassesProcessorsGradleTaskFactory {
    override fun createClassesProcessors(compileTask: AbstractCompile): List<ClassesProcessor> {
        if (!compileTask.project.isPluginApplied(KotlinJvmPluginId)) return emptyList()
        if (!compileTask.project.isPluginAppliedAndNotDisabled(MarkClassesDependentOnKotlinRuntimePlugin::class.java)) return emptyList()
        return listOf(MarkClassesDependentOnKotlinRuntimeClassesProcessor())
    }
}
