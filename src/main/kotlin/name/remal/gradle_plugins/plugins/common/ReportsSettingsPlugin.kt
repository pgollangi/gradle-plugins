package name.remal.gradle_plugins.plugins.common

import name.remal.gradle_plugins.dsl.BaseReflectiveProjectPlugin
import name.remal.gradle_plugins.dsl.Plugin
import name.remal.gradle_plugins.dsl.PluginAction
import name.remal.gradle_plugins.dsl.extensions.setDefaultDestinationForTask
import org.gradle.api.reporting.ConfigurableReport
import org.gradle.api.reporting.Reporting
import org.gradle.api.tasks.TaskContainer

@Plugin(
    id = "name.remal.reports-settings",
    description = "Plugin that configures reports.",
    tags = ["reports"]
)
class ReportsSettingsPlugin : BaseReflectiveProjectPlugin() {

    @PluginAction
    fun TaskContainer.`Setup all tasks with reports`() {
        all { task ->
            if (task is Reporting<*>) {
                task.reports?.all { report ->
                    if (report is ConfigurableReport) {
                        report.setDefaultDestinationForTask(task)
                    }
                }
            }
        }
    }

}
