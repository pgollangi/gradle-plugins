package name.remal.gradle_plugins.plugins.common

import name.remal.gradle_plugins.dsl.ApplyPluginClasses
import name.remal.gradle_plugins.dsl.ApplyPluginClassesAtTheEnd
import name.remal.gradle_plugins.dsl.BaseReflectiveProjectPlugin
import name.remal.gradle_plugins.dsl.Plugin
import name.remal.gradle_plugins.dsl.PluginAction
import name.remal.gradle_plugins.dsl.extensions.all
import name.remal.gradle_plugins.dsl.extensions.applyPlugin
import name.remal.gradle_plugins.dsl.extensions.contains
import name.remal.gradle_plugins.dsl.extensions.forMavenCentral
import name.remal.gradle_plugins.dsl.extensions.getOrNull
import name.remal.gradle_plugins.dsl.extensions.include
import name.remal.gradle_plugins.dsl.extensions.isGroupSet
import name.remal.gradle_plugins.dsl.extensions.isNotEmpty
import name.remal.gradle_plugins.dsl.extensions.isVersionSet
import name.remal.gradle_plugins.dsl.extensions.logDebug
import name.remal.gradle_plugins.dsl.extensions.releasesOnly
import name.remal.gradle_plugins.dsl.extensions.setAllprojectsGroup
import name.remal.gradle_plugins.dsl.extensions.setAllprojectsVersion
import name.remal.gradle_plugins.dsl.utils.MODULE_INFO_PATHS
import name.remal.gradle_plugins.dsl.utils.MODULE_NAME_MANIFEST_ATTRIBUTE
import name.remal.gradle_plugins.plugins.ci.CIExtension
import name.remal.gradle_plugins.plugins.dependencies.ComponentMetadataPlugin
import name.remal.gradle_plugins.plugins.dependencies.ConfigurationExtensionsPlugin
import name.remal.gradle_plugins.plugins.dependencies.DependenciesExtensionsPlugin
import name.remal.gradle_plugins.plugins.ide.eclipse.EclipsePluginId
import name.remal.gradle_plugins.plugins.ide.eclipse.EclipseSettingsPlugin
import name.remal.gradle_plugins.plugins.ide.idea.IdeaExtendedSettingsPlugin
import name.remal.gradle_plugins.plugins.ide.idea.IdeaPluginId
import name.remal.gradle_plugins.plugins.ide.idea.IdeaSettingsPlugin
import org.gradle.api.Project
import org.gradle.api.artifacts.ConfigurationContainer
import org.gradle.api.artifacts.ModuleDependency
import org.gradle.api.artifacts.dsl.RepositoryHandler
import org.gradle.api.file.CopySpec
import org.gradle.api.file.DuplicatesStrategy.WARN
import org.gradle.api.tasks.TaskContainer
import org.gradle.api.tasks.bundling.AbstractArchiveTask
import org.gradle.jvm.tasks.Jar

@Plugin(
    id = "name.remal.common-settings",
    description = "Plugin that applies common settings",
    tags = ["common"]
)
@ApplyPluginClasses(
    ConfigurationExtensionsPlugin::class,
    DependenciesExtensionsPlugin::class,
    ReportsSettingsPlugin::class,
    ComponentMetadataPlugin::class
)
@ApplyPluginClassesAtTheEnd(
    GradleWrapperSettingsPlugin::class
)
class CommonSettingsPlugin : BaseReflectiveProjectPlugin() {

    @PluginAction
    fun Project.`Apply IDE plugins`() {
        if (DisableOptionalPluginsMarker::class.java in extensions) {
            return
        }

        val isBuildOnCI = run {
            val ci = getOrNull(CIExtension::class.java) ?: return@run false
            return@run ci.isBuildOnCI
        }
        if (isBuildOnCI) {
            return
        }

        rootProject.allprojects {
            it.applyPlugin(IdeaPluginId)
            it.applyPlugin(IdeaSettingsPlugin::class.java)
            it.applyPlugin(IdeaExtendedSettingsPlugin::class.java)

            it.applyPlugin(EclipsePluginId)
            it.applyPlugin(EclipseSettingsPlugin::class.java)
        }
    }

    @PluginAction
    fun TaskContainer.`Set duplicates strategy to WARN for copying tasks`() {
        all {
            if (it is CopySpec) {
                it.duplicatesStrategy = WARN
            }
        }
    }

    @PluginAction
    fun TaskContainer.`Turn ON reproducible file order for archive tasks`() {
        all(AbstractArchiveTask::class.java) {
            it.isReproducibleFileOrder = true
        }
    }

    @PluginAction
    fun ConfigurationContainer.`Disable dependency transitivity if configuration is not transitive`() {
        all { conf ->
            conf.dependencies.all { dep ->
                if (!conf.isTransitive && dep is ModuleDependency) {
                    dep.isTransitive = false
                }
            }
        }
    }

    @PluginAction
    fun Project.`Copy project group to all sub-projects`() {
        if (isGroupSet) {
            setAllprojectsGroup(group.toString())
        }
    }

    @PluginAction
    fun Project.`Copy project version to all sub-projects`() {
        if (isVersionSet) {
            setAllprojectsVersion(version.toString())
        }
    }

    @PluginAction
    fun TaskContainer.`Set Multi-Release manifest attribute if the jar file is a multi-release jar`() {
        all(Jar::class.java) { task ->
            task.doFirst { _ ->
                val manifestAttributes = task.manifest.attributes
                val allSource = task.rootSpec.buildRootResolver().allSource

                val hasModuleInfo = allSource.include(*MODULE_INFO_PATHS.toTypedArray()).isNotEmpty
                if (hasModuleInfo) {
                    val moduleNameAttribute = MODULE_NAME_MANIFEST_ATTRIBUTE
                    task.logDebug("Removing {} manifest attribute, as the there is 'module-info.class' file", moduleNameAttribute)
                    manifestAttributes.remove(moduleNameAttribute)
                }

                val isMultiRelease = allSource.include("META-INF/versions/*/**").isNotEmpty
                if (isMultiRelease) {
                    val multiReleaseAttribute = "Multi-Release"
                    task.logDebug("Adding {} manifest attribute", multiReleaseAttribute)
                    manifestAttributes[multiReleaseAttribute] = "true"
                }
            }
        }
    }

    @PluginAction
    fun RepositoryHandler.`Disable SNAPSHOT artifacts resolution for Maven Central`() {
        forMavenCentral { it.releasesOnly() }
    }

}
