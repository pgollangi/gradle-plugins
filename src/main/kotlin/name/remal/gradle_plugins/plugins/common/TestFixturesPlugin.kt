package name.remal.gradle_plugins.plugins.common

import name.remal.gradle_plugins.dsl.BaseReflectiveProjectPlugin
import name.remal.gradle_plugins.dsl.Plugin

@Plugin(
    id = "name.remal.test-fixtures",
    description = "Plugin that marks the project as test fixtures for other projects",
    tags = ["common"]
)
class TestFixturesPlugin : BaseReflectiveProjectPlugin()
