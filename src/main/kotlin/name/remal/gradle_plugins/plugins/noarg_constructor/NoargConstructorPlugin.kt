package name.remal.gradle_plugins.plugins.noarg_constructor

import name.remal.gradle_plugins.dsl.ApplyPluginClasses
import name.remal.gradle_plugins.dsl.BaseReflectiveProjectPlugin
import name.remal.gradle_plugins.dsl.Plugin
import name.remal.gradle_plugins.dsl.WithPlugins
import name.remal.gradle_plugins.plugins.classes_processing.ClassesProcessingPlugin
import name.remal.gradle_plugins.plugins.java.JavaPluginId

@Plugin(
    id = "name.remal.noarg-constructor",
    description = "Plugin that adds no-arg protected synthetic constructor to all concrete classes where it's possible.",
    tags = ["java"]
)
@WithPlugins(JavaPluginId::class)
@ApplyPluginClasses(ClassesProcessingPlugin::class)
class NoargConstructorPlugin : BaseReflectiveProjectPlugin()
