package name.remal.gradle_plugins.plugins.code_quality.sonar.internal.impl

import name.remal.createDirectories
import name.remal.gradle_plugins.dsl.utils.code_quality.FindBugsReport
import name.remal.gradle_plugins.plugins.code_quality.sonar.internal.SONARLINT_TEMP_DIRS_WITH_CLEANUP
import name.remal.gradle_plugins.plugins.code_quality.sonar.internal.SonarLintInvoker
import name.remal.gradle_plugins.plugins.code_quality.sonar.internal.SonarSourceFile
import name.remal.orNull
import org.gradle.api.logging.Logger
import org.gradle.initialization.BuildCancellationToken
import org.sonarsource.sonarlint.core.StandaloneSonarLintEngineImpl
import org.sonarsource.sonarlint.core.client.api.common.Language
import org.sonarsource.sonarlint.core.client.api.common.RuleKey
import org.sonarsource.sonarlint.core.client.api.common.analysis.IssueListener
import org.sonarsource.sonarlint.core.client.api.standalone.StandaloneAnalysisConfiguration
import org.sonarsource.sonarlint.core.client.api.standalone.StandaloneGlobalConfiguration
import java.io.File

internal class SonarLintInvokerImpl : SonarLintInvoker {

    override fun invoke(
        sourceFiles: Iterable<SonarSourceFile>,
        excludedMessages: Iterable<String>,
        includedMessages: Iterable<String>,
        sonarProperties: Map<String, String>,
        ruleParameters: Map<String, Map<String, String>>,
        projectDir: File,
        pluginFiles: Iterable<File>,
        logger: Logger,
        buildCancellationToken: BuildCancellationToken
    ): FindBugsReport {
        return SONARLINT_TEMP_DIRS_WITH_CLEANUP.withTempDir { tempDir ->

            val logOutput = GradleLogOutput(logger)
            val progressMonitor = GradleProgressMonitor(logger, buildCancellationToken)
            val homeDir = tempDir.resolve("home").createDirectories()
            val workDir = tempDir.resolve("work").createDirectories()
            val engineConfig = StandaloneGlobalConfiguration.builder()
                .addEnabledLanguages(*Language.values())
                .apply {
                    pluginFiles.forEach { file ->
                        val pluginUrl = file.toURI().toURL()
                        logger.debug("Use Sonar plugin: {}", pluginUrl)
                        addPlugin(pluginUrl)
                    }
                }
                .setSonarLintUserHome(homeDir.toPath())
                .setWorkDir(workDir.toPath())
                .setLogOutput(logOutput)
                .build()

            val engine = StandaloneSonarLintEngineImpl(engineConfig)
            try {

                val analysisConfig = StandaloneAnalysisConfiguration.builder().apply {
                    setBaseDir(projectDir.toPath())
                    addInputFiles(sourceFiles.mapToClientInputFiles())
                    addExcludedRules(excludedMessages.map(RuleKey::parse))
                    addIncludedRules(includedMessages.map(RuleKey::parse))
                    putAllExtraProperties(sonarProperties)
                    ruleParameters.forEach { rule, props ->
                        addRuleParameters(RuleKey.parse(rule), props)
                    }
                }.build()
                logger.debug("Starting Sonar analysis with configuration:\n{}", analysisConfig)

                val findBugsReport = FindBugsReport()
                val issueListener = IssueListener { issue ->
                    findBugsReport.bug {
                        type(issue.ruleKey)
                        when (issue.severity.toUpperCase()) {
                            "BLOCKER" -> rankBlocker()
                            "CRITICAL" -> rankCritical()
                            "MAJOR" -> rankMajor()
                            "MINOR" -> rankMajor()
                            "INFO" -> rankInfo()
                        }
                        confidence(1)
                        message(issue.ruleName)
                        location {
                            (issue.inputFile?.getClientObject<Any?>() as? SonarSourceFile)?.relativePath?.let(::sourceFile)
                            issue.startLine?.let(::startLine)
                            issue.startLineOffset?.let(::startLineOffset)
                            issue.endLine?.let(::endLine)
                            issue.endLineOffset?.let(::endLineOffset)
                        }
                    }
                    findBugsReport.type(issue.ruleKey) {
                        htmlDescription(engine.getRuleDetails(issue.ruleKey).orNull?.htmlDescription)
                    }
                }

                engine.analyze(analysisConfig, issueListener, logOutput, progressMonitor)

                return@withTempDir findBugsReport

            } finally {
                engine.stop()
            }

        }
    }

}
