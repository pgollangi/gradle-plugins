package name.remal.gradle_plugins.plugins.code_quality.sonar.internal

import name.remal.gradle_plugins.dsl.utils.code_quality.FindBugsReport
import org.gradle.api.logging.Logger
import org.gradle.initialization.BuildCancellationToken
import java.io.File

internal interface SonarLintInvoker {

    @Suppress("LongParameterList", "kotlin:S107")
    fun invoke(
        sourceFiles: Iterable<SonarSourceFile>,
        excludedMessages: Iterable<String>,
        includedMessages: Iterable<String>,
        sonarProperties: Map<String, String>,
        ruleParameters: Map<String, Map<String, String>>,
        projectDir: File,
        pluginFiles: Iterable<File>,
        logger: Logger,
        buildCancellationToken: BuildCancellationToken
    ): FindBugsReport

}
