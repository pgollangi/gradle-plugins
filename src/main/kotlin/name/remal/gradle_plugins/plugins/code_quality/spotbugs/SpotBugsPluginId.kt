package name.remal.gradle_plugins.plugins.code_quality.spotbugs

import name.remal.gradle_plugins.dsl.PluginId

internal const val SPOTBUGS_PLUGIN_ID = "com.github.spotbugs"

object SpotBugsPluginId : PluginId(SPOTBUGS_PLUGIN_ID)
