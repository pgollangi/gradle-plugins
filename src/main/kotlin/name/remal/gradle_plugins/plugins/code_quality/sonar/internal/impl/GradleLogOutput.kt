package name.remal.gradle_plugins.plugins.code_quality.sonar.internal.impl

import org.gradle.api.logging.Logger
import org.sonarsource.sonarlint.core.client.api.common.LogOutput
import org.sonarsource.sonarlint.core.client.api.common.LogOutput.Level
import org.sonarsource.sonarlint.core.client.api.common.LogOutput.Level.DEBUG
import org.sonarsource.sonarlint.core.client.api.common.LogOutput.Level.ERROR
import org.sonarsource.sonarlint.core.client.api.common.LogOutput.Level.TRACE
import org.sonarsource.sonarlint.core.client.api.common.LogOutput.Level.WARN

internal class GradleLogOutput(private val logger: Logger) : LogOutput {
    override fun log(formattedMessage: String, level: Level) {
        when (level) {
            TRACE -> logger.trace(formattedMessage)
            DEBUG -> logger.debug(formattedMessage)
            WARN -> logger.warn(formattedMessage)
            ERROR -> logger.error(formattedMessage)
            else -> logger.info(formattedMessage)
        }
    }
}
