package name.remal.gradle_plugins.plugins.code_quality.sonar

import name.remal.filterNotNullValues
import name.remal.forInstantiatedWithPropagatedPackage
import name.remal.gradle_plugins.dsl.BuildTask
import name.remal.gradle_plugins.dsl.extensions.emptyFileCollection
import name.remal.gradle_plugins.dsl.extensions.forClassLoader
import name.remal.gradle_plugins.dsl.extensions.visitFiles
import name.remal.gradle_plugins.dsl.utils.code_quality.FindBugsReport
import name.remal.gradle_plugins.plugins.code_quality.BaseCodeQualityFindBugsResultsTask
import name.remal.gradle_plugins.plugins.code_quality.sonar.internal.SonarLintInvoker
import name.remal.gradle_plugins.plugins.code_quality.sonar.internal.SonarSourceFile
import name.remal.gradle_plugins.plugins.code_quality.sonar.internal.impl.SonarLintInvokerImpl
import org.gradle.api.file.FileCollection
import org.gradle.api.tasks.CacheableTask
import org.gradle.api.tasks.Classpath
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.Optional

@BuildTask
@CacheableTask
class SonarLint : BaseCodeQualityFindBugsResultsTask<SonarLintReports, SonarLintExtension>(), SonarPropertiesMixin<SonarLint> {

    override val findBugsXmlReportName get() = SonarLintReports::findBugsXml.name
    override val htmlReportName get() = SonarLintReports::html.name


    @get:Input
    @get:Optional
    var excludedMessages: MutableSet<String> = sortedSetOf()
        set(value) {
            field = value.toSortedSet()
        }

    fun excludeMessage(value: String) = excludeMessages(value)

    fun excludeMessages(vararg values: String) = apply {
        excludedMessages.addAll(values)
    }

    fun excludeMessages(values: Iterable<String>) = apply {
        excludedMessages.addAll(values)
    }

    init {
        handleExtension {
            it.excludes.also {
                exclude(it.sources)
                excludeMessages(it.messages)
            }
        }
    }


    @get:Input
    @get:Optional
    var includedMessages: MutableSet<String> = sortedSetOf()
        set(value) {
            field = value.toSortedSet()
        }

    fun includeMessage(value: String) = includeMessages(value)

    fun includeMessages(vararg values: String) = apply {
        includedMessages.addAll(values)
    }

    fun includeMessages(values: Iterable<String>) = apply {
        includedMessages.addAll(values)
    }

    init {
        handleExtension {
            it.includes.also {
                include(it.sources)
                includeMessages(it.messages)
            }
        }
    }


    @get:Input
    var isTestSources: Boolean = false


    @get:Input
    @get:Optional
    override var sonarProperties: MutableMap<String, String?> = sortedMapOf()
        set(value) {
            field = value.toSortedMap()
        }

    init {
        handleExtension {
            it.sonarProperties.forEach { key, value ->
                val currentValue = sonarProperties[key]
                if (currentValue == null) {
                    sonarProperties[key] = value
                }
            }
        }
    }


    @get:Input
    @get:Optional
    override var ruleParameters: MutableMap<String, MutableMap<String, String?>> = sortedMapOf()
        set(value) {
            field = value.toSortedMap()
        }

    init {
        handleExtension {
            it.ruleParameters.forEach { key, props ->
                val taskProps = ruleParameters.computeIfAbsent(key, { mutableMapOf() }).toMutableMap()
                props.forEach { propKey, propValue ->
                    val currentValue = taskProps[propKey]
                    if (currentValue == null) {
                        taskProps[propKey] = propValue
                    }
                }
            }
        }
    }


    @get:Classpath
    var sonarClasspath: FileCollection = project.emptyFileCollection()

    @get:Classpath
    var sonarPluginsClasspath: FileCollection = project.emptyFileCollection()


    override fun doAnalyze(): FindBugsReport {
        val inputFiles = mutableListOf<SonarSourceFile>()
        source.visitFiles { fileDetails ->
            inputFiles.add(
                SonarSourceFile(
                    fileDetails.file,
                    fileDetails.path,
                    isTestSources
                )
            )
        }

        lateinit var findBugsReport: FindBugsReport
        sonarClasspath.forClassLoader { classLoader ->
            classLoader.forInstantiatedWithPropagatedPackage(SonarLintInvoker::class.java, SonarLintInvokerImpl::class.java) { invoker ->
                findBugsReport = invoker.invoke(
                    inputFiles,
                    excludedMessages,
                    includedMessages,
                    sonarProperties.filterNotNullValues(),
                    ruleParameters.mapValues { it.value.filterNotNullValues() },
                    project.projectDir,
                    sonarPluginsClasspath,
                    logger,
                    buildCancellationToken
                )
            }
        }

        return findBugsReport
    }

}
