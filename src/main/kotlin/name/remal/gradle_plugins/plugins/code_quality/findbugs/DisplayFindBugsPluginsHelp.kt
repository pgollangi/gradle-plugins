package name.remal.gradle_plugins.plugins.code_quality.findbugs

import name.remal.gradle_plugins.dsl.BuildTask
import name.remal.gradle_plugins.dsl.extensions.onlyIfFirstTaskWithTheNameInGraph
import name.remal.gradle_plugins.dsl.utils.ProjectAware
import name.remal.gradle_plugins.plugins.code_quality.findbugs.extensions.FindBugsSubplugin
import name.remal.loadServices
import org.gradle.api.DefaultTask
import org.gradle.api.plugins.HelpTasksPlugin.HELP_GROUP
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.TaskAction

@BuildTask
class DisplayFindBugsPluginsHelp : DefaultTask() {

    init {
        group = HELP_GROUP
        onlyIfFirstTaskWithTheNameInGraph()
    }

    @Input
    lateinit var toolName: String

    @get:Input
    var toolExtensionName: String? = null
        get() = field ?: toolName.toLowerCase()

    @TaskAction
    protected fun displayHelp() {
        val findbugsSubplugins = loadServices(FindBugsSubplugin::class.java).toSortedSet()
        if (findbugsSubplugins.isEmpty()) {
            logger.lifecycle("No $toolName plugins found")
            return
        }

        logger.lifecycle("$toolName plugins:")
        findbugsSubplugins.forEach { ext ->
            if (ext is ProjectAware) {
                ext.project = project
            }
            logger.lifecycle("    $toolExtensionName.${ext.extensionName}() - adds ${ext.dependencyNotation} $toolName plugin")
        }
    }

}
