package name.remal.gradle_plugins.plugins.code_quality.jacoco

import name.remal.CompatibleMethodNotFoundException
import name.remal.gradle_plugins.dsl.BaseReflectiveProjectPlugin
import name.remal.gradle_plugins.dsl.Plugin
import name.remal.gradle_plugins.dsl.PluginAction
import name.remal.gradle_plugins.dsl.SimpleTestAdditionalGradleScript
import name.remal.gradle_plugins.dsl.WithPlugins
import name.remal.gradle_plugins.dsl.extensions.all
import name.remal.gradle_plugins.dsl.extensions.applyExcludeFromCodeCoverage
import name.remal.gradle_plugins.dsl.extensions.classDirectoriesCompatible
import name.remal.gradle_plugins.dsl.extensions.contains
import name.remal.gradle_plugins.dsl.extensions.doSetup
import name.remal.gradle_plugins.dsl.extensions.findDependencyConstraintVersion
import name.remal.gradle_plugins.dsl.extensions.get
import name.remal.gradle_plugins.dsl.extensions.getOrCreate
import name.remal.gradle_plugins.dsl.extensions.jacocoCoverageVerificationTaskName
import name.remal.gradle_plugins.dsl.extensions.jacocoReportTaskName
import name.remal.gradle_plugins.dsl.extensions.main
import name.remal.gradle_plugins.dsl.extensions.prepareExecutionData
import name.remal.gradle_plugins.dsl.extensions.sourceDirectoriesCompatible
import name.remal.gradle_plugins.plugins.java.JavaPluginId
import name.remal.gradle_plugins.utils.findVersionProperty
import name.remal.gradle_plugins.utils.getVersionProperty
import name.remal.uncheckedCast
import name.remal.version.Version
import name.remal.warn
import org.gradle.api.Project
import org.gradle.api.artifacts.ConfigurationContainer
import org.gradle.api.plugins.ExtensionContainer
import org.gradle.api.plugins.JavaBasePlugin.VERIFICATION_GROUP
import org.gradle.api.tasks.SourceSetContainer
import org.gradle.api.tasks.TaskContainer
import org.gradle.api.tasks.testing.Test
import org.gradle.testing.jacoco.plugins.JacocoPluginExtension
import org.gradle.testing.jacoco.plugins.JacocoTaskExtension
import org.gradle.testing.jacoco.tasks.JacocoCoverageVerification
import org.gradle.testing.jacoco.tasks.JacocoMerge
import org.gradle.testing.jacoco.tasks.JacocoReport
import org.gradle.testing.jacoco.tasks.JacocoReportBase

@Plugin(
    id = "name.remal.jacoco-settings",
    description = "Plugin that configures 'jacoco' plugin if it's applied.",
    tags = ["java", "jacoco", "coverage", "test"]
)
@WithPlugins(JacocoPluginId::class, JavaPluginId::class)
@SimpleTestAdditionalGradleScript(
    """
    dependencies { testImplementation 'junit:junit:4.12' }
    test {
        useJUnit()
        testLogging {
            showExceptions = true
            showCauses = true
            showStackTraces = true
            exceptionFormat = 'FULL'
            stackTraceFilters = ['GROOVY']
        }
    }
    file('src/test/java/T.java').with {
        parentFile.mkdirs()
        write('public class T { @org.junit.Test public void test() {} }', 'UTF-8')
    }
"""
)
class JacocoSettingsPlugin : BaseReflectiveProjectPlugin() {

    companion object {
        private val JACOCO_CONFIGURATION_NAME = Regex("jacoco([A-Z].*)?")
    }

    @PluginAction
    fun ExtensionContainer.`Update Jacoco version`(project: Project, configurations: ConfigurationContainer) {
        val toolExtension = this[JacocoPluginExtension::class.java]
        val constraintVersion = configurations.asSequence()
            .filter { JACOCO_CONFIGURATION_NAME.matches(it.name) }
            .mapNotNull { it.findDependencyConstraintVersion("org.jacoco") }
            .firstOrNull()
        if (constraintVersion != null) {
            toolExtension.toolVersion = constraintVersion
            return
        }

        val propertyVersion = project.findVersionProperty("jacoco") ?: project.getVersionProperty("jacoco-core")
        val toolVersionStr = toolExtension.toolVersion
        if (toolVersionStr.isNullOrEmpty()) {
            toolExtension.toolVersion = propertyVersion
            return
        }

        val toolVersion = Version.parseOrNull(toolVersionStr)
        val buildVersion = Version.parseOrNull(propertyVersion)
        if (toolVersion != null && buildVersion != null) {
            if (toolVersion < buildVersion) {
                toolExtension.toolVersion = buildVersion.toString()
            }
        }
    }

    @PluginAction
    fun TaskContainer.`Turn ON all reports`(extensions: ExtensionContainer) {
        all(JacocoReport::class.java) { task ->
            task.reports.all {
                it.isEnabled = true
            }
        }
    }

    @PluginAction
    fun TaskContainer.`Turn OFF fail on violation`() {
        all(JacocoCoverageVerification::class.java) {
            it.violationRules.isFailOnViolation = false
        }
    }

    @PluginAction("If jacoco task has not execution files set, it crashes. So let's use temp empty execution file by default.")
    fun TaskContainer.tempExecutionFileByDefault(project: Project, sourceSets: SourceSetContainer) {
        all(JacocoReportBase::class.java) {
            it.doSetup(Int.MAX_VALUE) {
                tryCompatibleMethodOrWarn { it.prepareExecutionData() }

                tryCompatibleMethodOrWarn {
                    if (it.sourceDirectoriesCompatible.isEmpty) it.sourceDirectoriesCompatible = sourceSets.main.allJava.sourceDirectories
                    if (it.classDirectoriesCompatible.isEmpty) it.classDirectoriesCompatible = sourceSets.main.output
                }

                tryCompatibleMethodOrWarn { it.applyExcludeFromCodeCoverage() }
            }
        }

        all(JacocoMerge::class.java) {
            it.doSetup(Int.MAX_VALUE) {
                tryCompatibleMethodOrWarn { it.prepareExecutionData() }
            }
        }
    }

    @PluginAction
    fun TaskContainer.`Setup jacoco for all Test tasks`(extensions: ExtensionContainer, tasks: TaskContainer) {
        all(Test::class.java) { testTask ->
            if (JacocoTaskExtension::class.java !in testTask.extensions) {
                val jacoco = extensions[JacocoPluginExtension::class.java]
                jacoco.applyTo(testTask)
            }

            val reportTask = tasks.getOrCreate(testTask.jacocoReportTaskName, JacocoReport::class.java).uncheckedCast<JacocoReport>().also { task ->
                testTask.finalizedBy(task)
                task.dependsOn(testTask)

                task.group = VERIFICATION_GROUP
                task.description = "Generates code coverage report for the ${testTask.name} task."
                task.executionData(testTask)
            }

            tasks.getOrCreate(testTask.jacocoCoverageVerificationTaskName, JacocoCoverageVerification::class.java).uncheckedCast<JacocoCoverageVerification>().also { task ->
                testTask.finalizedBy(task)
                task.dependsOn(testTask)
                task.dependsOn(reportTask)

                task.group = VERIFICATION_GROUP
                task.description = "Verifies code coverage metrics based on specified rules for the ${testTask.name} task."
                task.executionData(testTask)
            }
        }
    }


    private inline fun tryCompatibleMethodOrWarn(callback: () -> Unit) {
        try {
            callback()
        } catch (e: CompatibleMethodNotFoundException) {
            logger.warn(e)
        }
    }

}
