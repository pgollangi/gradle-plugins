package name.remal.gradle_plugins.plugins.code_quality.detekt

import name.remal.gradle_plugins.dsl.HighestPriorityPluginAction
import name.remal.gradle_plugins.dsl.Plugin
import name.remal.gradle_plugins.dsl.PluginAction
import name.remal.gradle_plugins.dsl.extensions.all
import name.remal.gradle_plugins.dsl.extensions.compileKotlinTaskName
import name.remal.gradle_plugins.dsl.extensions.defaultPropertyValue
import name.remal.gradle_plugins.dsl.extensions.findDependencyConstraintVersion
import name.remal.gradle_plugins.dsl.extensions.get
import name.remal.gradle_plugins.dsl.extensions.kotlin
import name.remal.gradle_plugins.dsl.extensions.withOneOfPlugin
import name.remal.gradle_plugins.dsl.extensions.withPlugin
import name.remal.gradle_plugins.dsl.utils.getPluginIdForLogging
import name.remal.gradle_plugins.plugins.code_quality.BaseCodeQualityReflectiveProjectPlugin
import name.remal.gradle_plugins.plugins.kotlin.KotlinAnyPluginId
import name.remal.gradle_plugins.utils.getVersionProperty
import org.gradle.api.GradleException
import org.gradle.api.Project
import org.gradle.api.artifacts.Configuration
import org.gradle.api.artifacts.ConfigurationContainer
import org.gradle.api.artifacts.dsl.DependencyHandler
import org.gradle.api.tasks.SourceSet
import org.gradle.api.tasks.TaskContainer
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

@Plugin(
    id = "name.remal.detekt",
    description = "Plugin that executes Detekt checks for Kotlin sources.",
    tags = ["kotlin", "detekt"]
)
class DetektPlugin : BaseCodeQualityReflectiveProjectPlugin<Detekt, DetektExtension>() {

    override val toolVersion: String
        get() {
            val constraintVersion = project.configurations.asSequence()
                .filter { it.name == configurationName }
                .mapNotNull { it.findDependencyConstraintVersion("io.gitlab.arturbosch.detekt") }
                .firstOrNull()
            if (constraintVersion != null) {
                return constraintVersion
            }

            return project.getVersionProperty("detekt")
        }

    override val isToolPluginsConfigurationEnabled: Boolean get() = true


    override fun Configuration.configureConfigurationExcludes() {
        // exclude nothing
    }

    override fun Configuration.configureConfiguration(extension: DetektExtension, dependencyHandler: DependencyHandler) {
        defaultDependencies { dependencies ->
            dependencies.add(dependencyHandler.createToolDependency("io.gitlab.arturbosch.detekt:detekt-cli:${extension.toolVersion}"))
        }
    }


    @PluginAction
    fun TaskContainer.configureDetektTasks(configurations: ConfigurationContainer) {
        all(Detekt::class.java) { task ->
            task.detektClasspath = configurations.toolConfiguration
            task.detektPluginsClasspath = configurations.toolPluginsConfiguration
            task.defaultPropertyValue(Detekt::configFile) { task.project[DetektExtension::class.java].configFile }
            task.defaultPropertyValue(Detekt::languageVersion) { task.project[DetektExtension::class.java].languageVersion }
            task.defaultPropertyValue(Detekt::jvmTarget) { task.project[DetektExtension::class.java].jvmTarget }
        }
    }

    override fun Detekt.configureTaskForSourceSet(sourceSet: SourceSet, isTest: Boolean) {
        project.withOneOfPlugin(KotlinAnyPluginId) {
            source(project.provider { sourceSet.kotlin })
            defaultPropertyValue(Detekt::classpath) { sourceSet.output + sourceSet.compileClasspath }
            defaultPropertyValue(Detekt::jvmTarget) {
                project.tasks.withType(KotlinCompile::class.java).findByName(sourceSet.compileKotlinTaskName)?.kotlinOptions?.jvmTarget
                    ?: project[DetektExtension::class.java].jvmTarget
            }
        }
    }


    @PluginAction
    fun TaskContainer.`Create 'writeDefaultDetektConfig' task`(configurations: ConfigurationContainer) {
        create("writeDefaultDetektConfig", WriteDefaultDetektConfig::class.java) { task ->
            task.detektClasspath = configurations.toolConfiguration
        }
    }

    @PluginAction
    fun TaskContainer.`Create 'updateDetektConfig' task`(configurations: ConfigurationContainer) {
        create("updateDetektConfig", UpdateDetektConfig::class.java) { task ->
            task.detektClasspath = configurations.toolConfiguration
        }
    }


    @HighestPriorityPluginAction
    fun Project.checkIfDefaultPluginApplied() {
        val plugin = "io.gitlab.arturbosch.detekt"
        withPlugin(plugin) {
            throw GradleException("${getPluginIdForLogging(DetektPlugin::class.java)} plugin is not compatible with $plugin")
        }
    }

}
