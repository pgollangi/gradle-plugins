@file:Suppress("OverridingDeprecatedMember")

package name.remal.gradle_plugins.plugins.code_quality.sonar.internal.impl

import name.remal.gradle_plugins.plugins.code_quality.sonar.internal.SonarSourceFile
import org.sonarsource.sonarlint.core.client.api.common.Language
import org.sonarsource.sonarlint.core.client.api.common.analysis.ClientInputFile

internal fun Iterable<SonarSourceFile>.mapToClientInputFiles(): List<ClientInputFile> = map { inputFile ->
    object : ClientInputFile {
        override fun getPath() = inputFile.absolutePath

        override fun isTest() = inputFile.isTest

        override fun getCharset() = inputFile.charset

        override fun language() = inputFile.language?.let { lang ->
            Language.values().firstOrNull { it.languageKey == lang }
        }

        override fun inputStream() = inputFile.file.inputStream()

        override fun contents() = inputFile.file.readText(inputFile.charset)

        override fun relativePath() = inputFile.relativePath

        override fun uri() = inputFile.file.toURI()

        @Suppress("UNCHECKED_CAST")
        override fun <G : Any?> getClientObject(): G = inputFile as G
    }
}
