package name.remal.gradle_plugins.plugins.code_quality.findbugs

import name.remal.gradle_plugins.dsl.PluginId

internal const val FINDBUGS_PLUGIN_ID = "findbugs"

object FindBugsPluginId : PluginId(FINDBUGS_PLUGIN_ID)
