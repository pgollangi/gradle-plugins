package name.remal.gradle_plugins.plugins.code_quality

import name.remal.KotlinAllOpen
import name.remal.Services.loadImplementationClasses
import name.remal.buildList
import name.remal.debug
import name.remal.error
import name.remal.gradle_plugins.dsl.extensions.ObjectMarker
import name.remal.gradle_plugins.dsl.extensions.addObjectMarker
import name.remal.gradle_plugins.dsl.extensions.doAfter
import name.remal.gradle_plugins.dsl.extensions.doSetup
import name.remal.gradle_plugins.dsl.utils.getGradleLogger
import name.remal.uncheckedCast
import org.gradle.api.Task

fun setupQualityTaskReporters(task: Task) {
    if (!task.addObjectMarker(ExecuteQualityTaskReportersMarker::class.java)) return

    task.doSetup(Int.MAX_VALUE) { curTask ->
        findReporters(curTask, QualityTaskReporter::class.java)
            .uncheckedCast<List<QualityTaskReporter<Task>>>()
            .forEach { it.configureTask(curTask) }
    }

    task.doAfter { curTask ->
        findReporter(curTask, QualityTaskConsoleReporter::class.java)
            ?.uncheckedCast<QualityTaskConsoleReporter<Task>>()
            ?.apply {
                try {
                    printReportOf(curTask)
                } catch (e: Exception) {
                    getGradleLogger(javaClass).error(e)
                }
            }

        findReporter(curTask, QualityTaskHtmlReporter::class.java)
            ?.uncheckedCast<QualityTaskHtmlReporter<Task>>()
            ?.apply {
                try {
                    writeHtmlReportFileOf(curTask)
                } catch (e: Exception) {
                    getGradleLogger(javaClass).error(e)
                }
            }
    }
}


private val reporters: List<QualityTaskReporter<*>> by lazy {
    buildList<QualityTaskReporter<*>> {
        val reporterClassesIterator = loadImplementationClasses(QualityTaskReporter::class.java).iterator()
        while (true) {
            try {
                if (!reporterClassesIterator.hasNext()) break
                val reporterClass = reporterClassesIterator.next()
                val reporter = reporterClass.getConstructor().newInstance()
                if (reporter.isEnabled) {
                    add(reporter)
                }

            } catch (e: Throwable) {
                getGradleLogger(QualityTaskReporter::class.java).debug(e)
            }
        }
    }.sortedBy { it.javaClass.name }
}

private fun <ReporterType : QualityTaskReporter<*>> findReporters(task: Task, reporterBaseType: Class<ReporterType>): List<ReporterType> {
    return reporters.filter {
        reporterBaseType.isAssignableFrom(it.javaClass) && it.taskType.isInstance(task)
    }.map { it.uncheckedCast<ReporterType>() }
}

private fun <ReporterType : QualityTaskReporter<*>> findReporter(task: Task, reporterBaseType: Class<ReporterType>): ReporterType? {
    return findReporters(task, reporterBaseType).firstOrNull()
}


@KotlinAllOpen
private class ExecuteQualityTaskReportersMarker : ObjectMarker
