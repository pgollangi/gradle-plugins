package name.remal.gradle_plugins.plugins.code_quality.detekt

import name.remal.createParentDirectories
import name.remal.gradle_plugins.dsl.extensions.emptyFileCollection
import name.remal.gradle_plugins.dsl.extensions.onlyIfFirstTaskWithTheNameInGraph
import name.remal.gradle_plugins.dsl.extensions.readAllBytes
import name.remal.gradle_plugins.dsl.utils.canonizeText
import name.remal.reflection.ExtendedURLClassLoader
import name.remal.reflection.ExtendedURLClassLoader.LoadingOrder.THIS_ONLY
import name.remal.removeAll
import name.remal.uncheckedCast
import org.gradle.api.DefaultTask
import org.gradle.api.GradleException
import org.gradle.api.file.FileCollection
import org.gradle.api.plugins.HelpTasksPlugin.HELP_GROUP
import org.gradle.api.tasks.Classpath
import org.gradle.api.tasks.InputFiles
import org.gradle.api.tasks.OutputFile
import org.gradle.api.tasks.TaskAction
import org.yaml.snakeyaml.DumperOptions
import org.yaml.snakeyaml.DumperOptions.FlowStyle.BLOCK
import org.yaml.snakeyaml.Yaml
import java.io.File
import java.io.InputStream
import java.nio.charset.StandardCharsets.UTF_8

abstract class BaseHandleDefaultDetektConfig : DefaultTask() {

    companion object {
        private const val DEFAULT_CONFIG_RESOURCE_NAME = "default-detekt-config.yml"


        private val yaml = Yaml(DumperOptions().apply {
            defaultFlowStyle = BLOCK
        })

        @JvmStatic
        protected fun serializeConfig(config: Map<String, Any>): String {
            val filteredConfig = config.toMutableMap()
            filteredConfig.removeAll(
                listOf(
                    "build",
                    "config",
                    "processors",
                    "console-reports",
                    "output-reports"
                )
            )

            val content = yaml.dump(filteredConfig)
                .replace(Regex("\\n(\\S)"), "\n\n$1")
            return content
        }

        @JvmStatic
        protected fun deserializeConfig(text: String): Map<String, Any> {
            return yaml.loadAs(text, Map::class.java).uncheckedCast()
        }

        @JvmStatic
        protected fun deserializeConfig(inpuStream: InputStream): Map<String, Any> {
            return yaml.loadAs(inpuStream, Map::class.java).uncheckedCast()
        }
    }


    init {
        group = HELP_GROUP
        onlyIfFirstTaskWithTheNameInGraph()
    }


    @InputFiles
    @Classpath
    var detektClasspath: FileCollection = project.emptyFileCollection()

    @get:OutputFile
    protected abstract val targetFile: File


    protected open fun handleDefaultConfigFile() {
        // do nothing by default
    }


    @TaskAction
    private fun doAction() {
        writeDefaultConfig()
        handleDefaultConfigFile()
        didWork = true
    }

    private fun writeDefaultConfig() {
        val detektClasspath = detektClasspath.toList()
        if (detektClasspath.isEmpty()) throw GradleException(WriteDefaultDetektConfig::detektClasspath.name + " is empty")

        val targetFile = targetFile.absoluteFile
        if (targetFile.isFile && targetFile.length() > 0) throw GradleException("File already exists: $targetFile")

        ExtendedURLClassLoader(THIS_ONLY, detektClasspath.map { it.toURI().toURL() }).use { classLoader ->
            val url = classLoader.getResource(DEFAULT_CONFIG_RESOURCE_NAME)
                ?: throw IllegalStateException("Detekt classpath doesn't have '$DEFAULT_CONFIG_RESOURCE_NAME' resource: " + detektClasspath.joinToString(", "))

            val connection = url.openConnection()
            connection.useCaches = false
            val bytes = connection.readAllBytes()

            var text = canonizeText(String(bytes, UTF_8))!!
            val config = deserializeConfig(text)
            text = serializeConfig(config)

            targetFile.createParentDirectories().writeText(text, UTF_8)
            logger.debug("Default Detekt config has been written to $targetFile")
        }
    }

}
