package name.remal.gradle_plugins.plugins.code_quality.jacoco

import name.remal.asString
import name.remal.clearNamespaces
import name.remal.createParentDirectories
import name.remal.gradle_plugins.dsl.ApplyPluginClasses
import name.remal.gradle_plugins.dsl.BaseReflectiveProjectPlugin
import name.remal.gradle_plugins.dsl.BuildTask
import name.remal.gradle_plugins.dsl.HighestPriorityPluginAction
import name.remal.gradle_plugins.dsl.Plugin
import name.remal.gradle_plugins.dsl.PluginAction
import name.remal.gradle_plugins.dsl.PluginActionsGroup
import name.remal.gradle_plugins.dsl.extensions.all
import name.remal.gradle_plugins.dsl.extensions.applyPlugin
import name.remal.gradle_plugins.dsl.extensions.doSetup
import name.remal.gradle_plugins.dsl.extensions.get
import name.remal.gradle_plugins.dsl.extensions.isBuildSrcProject
import name.remal.gradle_plugins.plugins.common.CommonSettingsPlugin
import name.remal.loadProperties
import name.remal.setNoOpEntityResolver
import name.remal.setNoValidatingXMLReaderFactory
import name.remal.store
import org.gradle.api.DefaultTask
import org.gradle.api.Project
import org.gradle.api.Project.DEFAULT_BUILD_DIR_NAME
import org.gradle.api.file.FileCollection
import org.gradle.api.reporting.ReportingExtension.DEFAULT_REPORTS_DIR_NAME
import org.gradle.api.tasks.CacheableTask
import org.gradle.api.tasks.InputFiles
import org.gradle.api.tasks.OutputFile
import org.gradle.api.tasks.PathSensitive
import org.gradle.api.tasks.PathSensitivity.ABSOLUTE
import org.gradle.api.tasks.TaskAction
import org.gradle.api.tasks.TaskContainer
import org.gradle.initialization.DefaultSettings.DEFAULT_BUILD_SRC_DIR
import org.gradle.testing.jacoco.tasks.JacocoReport
import org.jdom2.input.SAXBuilder
import java.io.File
import java.util.Locale.ENGLISH
import java.util.Properties

const val MERGED_JACOCO_REPORT_PROPERTIES_FILE_RELATIVE_PATH = "$DEFAULT_BUILD_DIR_NAME/$DEFAULT_REPORTS_DIR_NAME/merged-jacoco.properties"

const val COLLECT_MERGED_JACOCO_REPORT_PROPERTIES_FILE_TASK_NAME = "collectMergedJacocoReportPropertiesFile"
const val DISPLAY_MERGED_JACOCO_REPORT_TASK_NAME = "displayMergedJacocoReport"

@Plugin(
    id = "name.remal.merged-jacoco-report",
    description = "Plugin that merges all jacoco reports in current project and all subprojects.",
    tags = ["java", "jacoco", "coverage", "test"]
)
@ApplyPluginClasses(CommonSettingsPlugin::class)
class MergedJacocoReportPlugin : BaseReflectiveProjectPlugin() {

    @PluginActionsGroup(order = Int.MIN_VALUE)
    inner class `For current project and all subprojects` {

        @HighestPriorityPluginAction("Apply `name.remal.merge-jacoco-reports` plugin")
        fun Project.applyMergeJacocoReportsPlugin() {
            allprojects { it.applyPlugin(MergeJacocoReportsPlugin::class.java) }
        }

        @PluginAction
        fun Project.`Turn ON XML report for jacocoMergeReport task`() {
            allprojects {
                it.tasks.all(JacocoReport::class.java, JACOCO_MERGE_REPORT_TASK_NAME) {
                    it.doSetup { it.reports.xml.isEnabled = true }
                }
            }
        }

    }

    @PluginAction(order = 1)
    fun TaskContainer.`Create 'collectMergedJacocoReportPropertiesFile' task`() {
        create(COLLECT_MERGED_JACOCO_REPORT_PROPERTIES_FILE_TASK_NAME, CollectMergedJacocoReportPropertiesFile::class.java) { collectTask ->
            collectTask.project.allprojects {
                it.tasks.all(JacocoReport::class.java, JACOCO_MERGE_REPORT_TASK_NAME, collectTask::addJacocoXmlReportTask)
            }
        }
    }

    @PluginAction(order = 2)
    fun TaskContainer.`Create 'displayMergedJacocoReport' task`() {
        val collectTask = this[CollectMergedJacocoReportPropertiesFile::class.java, COLLECT_MERGED_JACOCO_REPORT_PROPERTIES_FILE_TASK_NAME]
        create(DISPLAY_MERGED_JACOCO_REPORT_TASK_NAME, DisplayMergedJacocoReport::class.java) {
            it.dependsOn(collectTask)
            it.doSetup {
                it.mergedJacocoReportPropertiesFiles.add(collectTask.mergedJacocoReportPropertiesFile)
                if (!it.project.isBuildSrcProject) {
                    it.mergedJacocoReportPropertiesFiles.add(it.project.rootDir.resolve(DEFAULT_BUILD_SRC_DIR).resolve(MERGED_JACOCO_REPORT_PROPERTIES_FILE_RELATIVE_PATH))
                }
            }
        }
    }

}


private const val COVERED_LINES_PROPERTY_NAME = "covered"
private const val MISSED_LINES_PROPERTY_NAME = "missed"


@BuildTask
@CacheableTask
class CollectMergedJacocoReportPropertiesFile : DefaultTask() {

    init {
        description = "Collect merged jacoco report properties"
    }

    @get:OutputFile
    val mergedJacocoReportPropertiesFile: File
        get() = File(project.projectDir, MERGED_JACOCO_REPORT_PROPERTIES_FILE_RELATIVE_PATH).absoluteFile

    @InputFiles
    @PathSensitive(ABSOLUTE)
    protected var jacocoXmlReportFiles: FileCollection = project.files()

    fun addJacocoXmlReport(file: File) {
        jacocoXmlReportFiles += project.files(file)
    }

    fun addJacocoXmlReportTask(jacocoReportTask: JacocoReport) {
        dependsOn(jacocoReportTask)
        jacocoXmlReportFiles += project.files(project.provider { jacocoReportTask.reports.xml.destination })
    }

    @TaskAction
    protected fun doCollect() {
        var coveredLines = 0L
        var missedLines = 0L

        if (!jacocoXmlReportFiles.isEmpty) {
            val saxBuilder = SAXBuilder().apply {
                setNoValidatingXMLReaderFactory()
                setNoOpEntityResolver()
            }

            jacocoXmlReportFiles.forEach forEachReportFile@{ jacocoXmlReportFile ->
                if (!jacocoXmlReportFile.isFile) return@forEachReportFile
                saxBuilder.build(jacocoXmlReportFile)
                    .clearNamespaces()
                    .rootElement
                    .getChildren("counter")
                    .forEach forEachCounter@{ counterNode ->
                        if (true != counterNode.getAttributeValue("type")?.equals("LINE", true)) return@forEachCounter
                        val curCoveredLines = counterNode.getAttributeValue("covered")?.toLongOrNull()
                        val curMissedLines = counterNode.getAttributeValue("missed")?.toLongOrNull()
                        if (curCoveredLines != null && curMissedLines != null) {
                            coveredLines += curCoveredLines
                            missedLines += curMissedLines
                        } else {
                            logger.warn("Unsupported report node: {}", counterNode.asString(true))
                        }
                    }
            }
        }

        Properties().apply {
            setProperty(COVERED_LINES_PROPERTY_NAME, coveredLines.toString())
            setProperty(MISSED_LINES_PROPERTY_NAME, missedLines.toString())
            store(mergedJacocoReportPropertiesFile.createParentDirectories())
        }
    }

}


@BuildTask
class DisplayMergedJacocoReport : DefaultTask() {

    init {
        description = "Display merged jacoco report"
    }

    @InputFiles
    @PathSensitive(ABSOLUTE)
    val mergedJacocoReportPropertiesFiles = mutableListOf<File>()

    @TaskAction
    protected fun doDisplay() {
        var coveredLines = 0L
        var missedLines = 0L
        mergedJacocoReportPropertiesFiles.forEach { propsFile ->
            if (!propsFile.exists()) return@forEach
            loadProperties(propsFile).apply {
                coveredLines += this[COVERED_LINES_PROPERTY_NAME]?.toString()?.toLongOrNull() ?: 0L
                missedLines += this[MISSED_LINES_PROPERTY_NAME]?.toString()?.toLongOrNull() ?: 0L
            }
        }


        val totalLines = coveredLines + missedLines
        if (0L == totalLines) return

        logger.lifecycle("Merged code coverage: %.2f%% (%d of %d lines)".format(ENGLISH, 100.0 * coveredLines / totalLines, coveredLines, totalLines))
    }

}
