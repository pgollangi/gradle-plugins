@file:Suppress("DEPRECATION")

package name.remal.gradle_plugins.plugins.code_quality.findbugs

import name.remal.CLASS_FILE_NAME_SUFFIX
import name.remal.gradle_plugins.dsl.DoNotGenerateSimpleTest
import name.remal.gradle_plugins.dsl.Plugin
import name.remal.gradle_plugins.dsl.PluginAction
import name.remal.gradle_plugins.dsl.WithPlugins
import name.remal.gradle_plugins.dsl.extensions.all
import name.remal.gradle_plugins.dsl.extensions.doSetup
import name.remal.gradle_plugins.dsl.extensions.visitFiles
import name.remal.gradle_plugins.dsl.utils.isAnnotatedBy
import name.remal.gradle_plugins.plugins.java.JavaPluginId
import name.remal.gradle_plugins.plugins.kotlin.KotlinJvmPluginId
import name.remal.gradle_plugins.utils.getVersionProperty
import name.remal.resourceNameToClassName
import org.gradle.api.file.FileCollection
import org.gradle.api.model.ObjectFactory
import org.gradle.api.plugins.ExtensionContainer
import org.gradle.api.plugins.quality.FindBugs
import org.gradle.api.plugins.quality.FindBugsExtension
import org.gradle.api.tasks.TaskContainer
import java.io.File
import javax.inject.Inject

@Plugin(
    id = "name.remal.findbugs-settings",
    description = "Plugin that configures 'findbugs' plugin if it's applied.",
    tags = ["java", "findbugs"]
)
@WithPlugins(FindBugsPluginId::class, JavaPluginId::class)
@DoNotGenerateSimpleTest
class FindBugsSettingsPlugin @Inject constructor(
    objectFactory: ObjectFactory
) : BaseFindBugsSettingsPlugin<FindBugsExtension, FindBugs>(objectFactory) {

    override val toolName: String = "FindBugs"

    override val toolLatestVersion: String get() = project.getVersionProperty("findbugs")

    override val toolArtifactGroup: String = "com.google.code.findbugs"
    override val toolArtifactIds: Set<String> = setOf("findbugs", "annotations", "jsr305")

    override var FindBugs.excludeFilterFile: File?
        get() = excludeFilter
        set(value) {
            excludeFilter = value
        }

    override val FindBugs.classesDirs: FileCollection get() = classes


    @PluginAction
    @WithPlugins(KotlinJvmPluginId::class)
    fun ExtensionContainer.`Don't check Kotlin sources`() {
        globalExcludes.sources("*.kt", "*.kts")
    }

    @PluginAction
    @WithPlugins(KotlinJvmPluginId::class)
    fun TaskContainer.`Don't check Kotlin classes`() {
        all(taskType) {
            it.doSetup(Int.MAX_VALUE - 1) { task ->
                val taskExcludes = task.taskExcludes
                task.classesDirs.asFileTree.visitFiles { details ->
                    if (details.name.endsWith(CLASS_FILE_NAME_SUFFIX)) {
                        val isKotlinClass = isAnnotatedBy(details.file.readBytes(), "kotlin.Metadata")
                        if (isKotlinClass) {
                            taskExcludes.className(resourceNameToClassName(details.relativePath.toString()))
                        }
                    }
                }
            }
        }
    }

}
