package name.remal.gradle_plugins.plugins.code_quality

import org.gradle.api.plugins.quality.CodeQualityExtension

abstract class BaseCodeQualityExtension : CodeQualityExtension() {

    @Volatile
    internal var resolvedToolVersionRetriever: (() -> String?)? = null

    internal val resolvedToolVersion: String? get() = resolvedToolVersionRetriever?.invoke()

}
