@file:Suppress("DEPRECATION")

package name.remal.gradle_plugins.plugins.code_quality.findbugs

import name.remal.gradle_plugins.api.AutoService
import name.remal.gradle_plugins.plugins.code_quality.QualityTaskReporter
import org.gradle.api.plugins.quality.FindBugs
import org.gradle.api.plugins.quality.FindBugsExtension
import org.gradle.api.plugins.quality.FindBugsReports

@AutoService(QualityTaskReporter::class)
class FindBugsReporter : BaseFindBugsReporter<FindBugs, FindBugsReports, FindBugsExtension>()
