package name.remal.gradle_plugins.plugins.vcs

import name.remal.default
import name.remal.gradle_plugins.dsl.BuildTask
import name.remal.gradle_plugins.dsl.extensions.get
import name.remal.gradle_plugins.dsl.extensions.invoke
import name.remal.gradle_plugins.dsl.extensions.logLifecycle
import name.remal.gradle_plugins.dsl.extensions.requirePlugin
import name.remal.gradle_plugins.dsl.extensions.shouldRunAfter
import name.remal.gradle_plugins.dsl.extensions.sourceSetTaskNames
import name.remal.gradle_plugins.plugins.generate_sources.BaseGenerateTask
import org.gradle.api.Action
import org.gradle.api.DefaultTask
import org.gradle.api.plugins.JavaBasePlugin.BUILD_TASK_NAME
import org.gradle.api.plugins.JavaPlugin.TEST_TASK_NAME
import org.gradle.api.plugins.JavaPluginConvention
import org.gradle.api.publish.ivy.tasks.GenerateIvyDescriptor
import org.gradle.api.publish.ivy.tasks.PublishToIvyRepository
import org.gradle.api.publish.maven.tasks.AbstractPublishToMaven
import org.gradle.api.publish.maven.tasks.GenerateMavenPom
import org.gradle.api.publish.tasks.GenerateModuleMetadata
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.Internal
import org.gradle.api.tasks.Optional
import org.gradle.api.tasks.SourceTask
import org.gradle.api.tasks.TaskAction
import org.gradle.api.tasks.VerificationTask
import org.gradle.api.tasks.compile.AbstractCompile
import org.gradle.api.tasks.testing.AbstractTestTask

@BuildTask
abstract class BaseCreateTagTask : DefaultTask() {

    @get:Input
    @get:Optional
    protected abstract val tagName: String?

    @Internal
    var failIfTagExists: Boolean = true

    @Internal
    var failIfTagExistsOnCurrentCommit: Boolean = false

    @Internal
    var dryRun: Boolean = false


    @Internal
    val onTagCreatedHandlers: MutableList<Action<String>> = mutableListOf()

    fun onTagCreated(handler: Action<String>) = apply { onTagCreatedHandlers.add(handler) }
    fun onTagCreated(handler: (tagName: String) -> Unit) = onTagCreated(Action(handler))

    @Internal
    val onTagNotCreatedHandlers: MutableList<Action<String>> = mutableListOf()

    fun onTagNotCreated(handler: Action<String>) = apply { onTagNotCreatedHandlers.add(handler) }
    fun onTagNotCreated(handler: (tagName: String) -> Unit) = onTagNotCreated(Action(handler))


    private val vcsOperations: VcsOperations by lazy { project[VcsOperationsExtension::class.java] }

    init {
        requirePlugin(VcsOperationsPlugin::class.java)

        shouldRunAfter {
            project.rootProject.allprojects.asSequence()
                .flatMap { project ->
                    project.tasks.asSequence()
                        .filter { task ->
                            if (task.name == BUILD_TASK_NAME) return@filter true
                            if (task.name == TEST_TASK_NAME) return@filter true
                            if (task is BaseGenerateTask) return@filter true
                            if (task is AbstractCompile) return@filter true
                            if (task is AbstractTestTask) return@filter true
                            if (task is SourceTask) return@filter true
                            if (task is VerificationTask) return@filter true
                            if (project.extensions.findByType(JavaPluginConvention::class.java)?.sourceSets?.any { task.name in it.sourceSetTaskNames } == true) return@filter true
                            return@filter false
                        }
                }
                .toList()
        }

        project.rootProject.allprojects { project ->
            project.tasks.all { task ->
                if (task is GenerateModuleMetadata
                    || task is GenerateMavenPom
                    || task is AbstractPublishToMaven
                    || task is GenerateIvyDescriptor
                    || task is PublishToIvyRepository
                ) {
                    task.shouldRunAfter(this)
                }
            }
        }
    }

    @TaskAction
    @Suppress("ComplexMethod")
    protected fun createTag() {
        val tagName = this.tagName
        if (tagName == null) return

        val allTagNames: Set<String> by lazy(LazyThreadSafetyMode.NONE, vcsOperations::getAllTagNames)
        if (!failIfTagExists || tagName !in allTagNames) {
            if (vcsOperations.getCurrentCommit()?.tags?.contains(tagName) == true) {
                if (failIfTagExistsOnCurrentCommit) {
                    onTagNotCreatedHandlers.forEach { it(tagName) }
                    throw VcsTagExistsException("Current commit already has '$tagName' tag")
                } else {
                    logger.lifecycle("Current commit already has '{}' tag", tagName)
                    onTagNotCreatedHandlers.forEach { it(tagName) }
                    return
                }
            }

            if (!dryRun) {
                vcsOperations.createTagForCurrentCommit(tagName)
                logLifecycle("Tag has successfully been created for current commit: '{}'", tagName)

            } else {
                logger.lifecycle(
                    "{} GIT tag '{}' for commit {}",
                    if (tagName in allTagNames) "Updating" else "Creating",
                    tagName,
                    vcsOperations.getCurrentCommit()?.id.default("HEAD")
                )
            }
            onTagCreatedHandlers.forEach { it(tagName) }

        } else {
            onTagNotCreatedHandlers.forEach { it(tagName) }
            throw VcsTagExistsException("Tag '$tagName' already exists")
        }

        didWork = true
    }

}
