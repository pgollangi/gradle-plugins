package name.remal.gradle_plugins.plugins.vcs

data class TagsWithDepth(
    val tagNames: Set<String>,
    val depth: Int
)
