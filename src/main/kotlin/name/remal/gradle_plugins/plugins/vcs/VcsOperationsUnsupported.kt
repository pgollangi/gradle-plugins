package name.remal.gradle_plugins.plugins.vcs

import name.remal.emptyStream
import name.remal.gradle_plugins.dsl.utils.getGradleLogger
import java.io.File

class VcsOperationsUnsupported(context: Any? = "project") : VcsOperations() {

    companion object {
        private val logger = getGradleLogger(VcsOperationsUnsupported::class.java)
    }

    private val notSupportedMessage = "VCS operations are not supported for $context"

    override val vcsRootDir get() = throw UnsupportedOperationException(notSupportedMessage)

    override val trueMasterBranch get() = ""

    override val trueCurrentBranch: String? get() = null

    override val isCommitted get() = false

    override var commitAuthor: CommitAuthor? = null

    override fun walkCommits() = emptyStream<Commit>()

    override fun commitFiles(message: String, files: Collection<File>) = throw UnsupportedOperationException(notSupportedMessage)

    override fun getAllTagNames() = emptySet<String>()

    override fun createTag(commitId: String, tagName: String, message: String) = throw UnsupportedOperationException(notSupportedMessage)

    override fun findTagWithDepth(predicate: (tagName: String) -> Boolean): TagsWithDepth? = null

    override fun setUnauthorizedRemoteURI(uri: String) {
        logger.error(notSupportedMessage)
    }

    override fun setUsernamePasswordAuth(username: String, password: CharArray) {
        logger.error(notSupportedMessage)
    }

    override fun setSSHAuth(privateKeyFile: File, password: CharArray?) {
        logger.error(notSupportedMessage)
    }

}
