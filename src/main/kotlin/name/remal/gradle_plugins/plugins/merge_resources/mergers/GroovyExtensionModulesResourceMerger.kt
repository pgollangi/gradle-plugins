package name.remal.gradle_plugins.plugins.merge_resources.mergers

import name.remal.SERVICE_FILE_BASE_PATH
import name.remal.gradle_plugins.api.AutoService
import name.remal.gradle_plugins.dsl.extensions.isPluginApplied
import name.remal.gradle_plugins.dsl.utils.getGradleLogger
import name.remal.gradle_plugins.plugins.java.JavaPluginId
import name.remal.gradle_plugins.plugins.merge_resources.BaseResourceMerger
import name.remal.gradle_plugins.plugins.merge_resources.ResourceMerger
import name.remal.gradle_plugins.plugins.merge_resources.ResourceMergerFactory
import name.remal.loadProperties
import name.remal.store
import org.gradle.api.Project
import org.gradle.api.Task
import org.gradle.api.file.RelativePath
import java.io.File
import java.util.Properties

class GroovyExtensionModulesResourceMerger(private val project: Project) : BaseResourceMerger("$SERVICE_FILE_BASE_PATH/$GROOVY_EXTENSION_MODULE_SERVICE") {

    companion object {
        private const val GROOVY_EXTENSION_MODULE_SERVICE = "org.codehaus.groovy.runtime.ExtensionModule"
        private const val MODULE_NAME_PROPERTY = "moduleName"
        private const val MODULE_VERSION_PROPERTY = "moduleVersion"

        private val logger = getGradleLogger(GroovyExtensionModulesResourceMerger::class.java)
    }

    override fun mergeFiles(relativePath: RelativePath, files: List<File>, mergedFilesDir: File) {
        val moduleNames = mutableSetOf<String>()
        val moduleVersions = mutableSetOf<String>()
        val mergedProps = Properties()
        files.forEach { file ->
            val props = loadProperties(file)
            props.forEach forEachProp@{ key, value ->
                if (key == null || value == null) return@forEachProp
                if (MODULE_NAME_PROPERTY == key) moduleNames.add(value.toString())
                if (MODULE_VERSION_PROPERTY == key) moduleVersions.add(value.toString())
                if (key !in mergedProps) {
                    mergedProps[key] = value.toString()
                } else {
                    mergedProps[key] = mergedProps[key].toString() + "," + value.toString()
                }
            }
        }

        if (moduleNames.size >= 2) {
            logger.warn("The project's Groovy extension modules have different module names: {}", moduleNames.joinToString(", "))
        }
        if (moduleVersions.size >= 2) {
            logger.warn("The project's Groovy extension modules have different module versions: {}", moduleVersions.joinToString(", "))
        }

        mergedProps[MODULE_NAME_PROPERTY] = moduleNames.single()
        mergedProps[MODULE_VERSION_PROPERTY] = moduleVersions.single()

        val targetFile = relativePath.getFile(mergedFilesDir)
        mergedProps.store(targetFile)
    }

}

@AutoService
class GroovyExtensionModulesResourceMergerFactory : ResourceMergerFactory {
    override fun createResourceMerger(task: Task): List<ResourceMerger> {
        if (!task.project.isPluginApplied(JavaPluginId)) return emptyList()
        return listOf(GroovyExtensionModulesResourceMerger(task.project))
    }
}
