package name.remal.gradle_plugins.plugins.merge_resources.mergers

import name.remal.SERVICE_FILE_BASE_PATH
import name.remal.createParentDirectories
import name.remal.gradle_plugins.api.AutoService
import name.remal.gradle_plugins.dsl.extensions.isPluginApplied
import name.remal.gradle_plugins.plugins.java.JavaPluginId
import name.remal.gradle_plugins.plugins.merge_resources.BaseResourceMerger
import name.remal.gradle_plugins.plugins.merge_resources.ResourceMerger
import name.remal.gradle_plugins.plugins.merge_resources.ResourceMergerFactory
import name.remal.use
import org.gradle.api.Task
import org.gradle.api.file.RelativePath
import java.io.File
import java.nio.charset.StandardCharsets.UTF_8
import kotlin.Int.Companion.MAX_VALUE

class JavaServicesResourceMerger : BaseResourceMerger("$SERVICE_FILE_BASE_PATH/*") {

    override fun mergeFiles(relativePath: RelativePath, files: List<File>, mergedFilesDir: File) {
        val targetFile = relativePath.getFile(mergedFilesDir)
        targetFile.createParentDirectories().writer(UTF_8).use { writer ->
            files.asSequence()
                .map { it.readText(UTF_8) }
                .flatMap { it.splitToSequence('\n', '\r') }
                .map { it.substringBefore('#') }
                .map(String::trim)
                .filter(String::isNotEmpty)
                .distinct()
                .forEach { writer.write("$it\n") }
        }
    }

    override fun getOrder() = MAX_VALUE

}

@AutoService
class JavaServicesResourceMergerFactory : ResourceMergerFactory {
    override fun createResourceMerger(task: Task): List<ResourceMerger> {
        if (!task.project.isPluginApplied(JavaPluginId)) return emptyList()
        return listOf(JavaServicesResourceMerger())
    }
}
