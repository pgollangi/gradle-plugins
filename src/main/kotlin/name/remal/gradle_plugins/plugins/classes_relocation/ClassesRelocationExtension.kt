package name.remal.gradle_plugins.plugins.classes_relocation

import name.remal.asSynchronized
import name.remal.gradle_plugins.dsl.Extension
import name.remal.gradle_plugins.dsl.extensions.all
import name.remal.gradle_plugins.dsl.extensions.forEach
import name.remal.gradle_plugins.dsl.extensions.javaPackageName
import name.remal.gradle_plugins.dsl.utils.ClassInternalName
import name.remal.gradle_plugins.dsl.utils.ClassName
import org.gradle.api.Project
import org.gradle.api.tasks.TaskInputs
import org.gradle.api.tasks.compile.AbstractCompile
import java.util.concurrent.ConcurrentMap
import java.util.concurrent.ConcurrentSkipListMap
import kotlin.reflect.KProperty1

@Extension
class ClassesRelocationExtension(private val project: Project) {

    private var _relocatedClassesPackageName: String? = null

    var relocatedClassesPackageName: String
        get() = _relocatedClassesPackageName ?: project.javaPackageName + ".internal._relocated"
        set(value) {
            _relocatedClassesPackageName = value

            project.tasks.forEach(AbstractCompile::class.java) { it.setProps() }
        }


    init {
        project.tasks.all(AbstractCompile::class.java) { it.setProps() }
    }

    private fun AbstractCompile.setProps() {
        inputs.prop(ClassesRelocationExtension::relocatedClassesPackageName)
    }

    private fun TaskInputs.prop(property: KProperty1<ClassesRelocationExtension, *>) {
        val name = ClassesRelocationExtension::class.java.simpleName + '-' + property.name
        val value = property.get(this@ClassesRelocationExtension)
        property(name, value)
    }


    @Suppress("VariableNaming")
    val _relocatedClassNames: ConcurrentMap<ClassName, ClassName> = ConcurrentSkipListMap()

    @Suppress("VariableNaming")
    val _relocatedClassSources: MutableMap<ClassInternalName, ClassInternalName> = mutableMapOf<String, String>().asSynchronized()

}
