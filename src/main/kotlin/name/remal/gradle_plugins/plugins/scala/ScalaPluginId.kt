package name.remal.gradle_plugins.plugins.scala

import name.remal.gradle_plugins.dsl.PluginId

object ScalaPluginId : PluginId("scala")
