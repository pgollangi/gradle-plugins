package name.remal.gradle_plugins.plugins.dependencies

import name.remal.gradle_plugins.dsl.ApplyPluginClasses
import name.remal.gradle_plugins.dsl.BaseReflectiveProjectPlugin
import name.remal.gradle_plugins.dsl.CreateExtensionsPluginAction
import name.remal.gradle_plugins.dsl.Plugin
import name.remal.gradle_plugins.plugins.common.CommonSettingsPlugin
import org.gradle.api.Project
import org.gradle.api.artifacts.ConfigurationContainer
import org.gradle.api.plugins.ExtensionContainer

@Plugin(
    id = "name.remal.transitive-dependencies",
    description = "Plugin that makes easier to configure transitive dependencies.",
    tags = ["common", "dependencies"]
)
@ApplyPluginClasses(CommonSettingsPlugin::class)
class TransitiveDependenciesPlugin : BaseReflectiveProjectPlugin() {

    @CreateExtensionsPluginAction
    fun ExtensionContainer.`Create 'transitiveDependencies' extension`(project: Project, configurations: ConfigurationContainer): TransitiveDependenciesExtension {
        return create(
            "transitiveDependencies",
            TransitiveDependenciesExtension::class.java,
            project,
            configurations
        )
    }

}
