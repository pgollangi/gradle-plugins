package name.remal.gradle_plugins.plugins.dependencies

interface StaticAnalysisTransitiveDependencies {
    val dependencyNotations: List<String>
}
