package name.remal.gradle_plugins.plugins.dependencies

import com.fasterxml.jackson.core.type.TypeReference
import name.remal.gradle_plugins.dsl.utils.getGradleLogger
import name.remal.gradle_plugins.dsl.utils.getPluginIdForLogging
import name.remal.gradle_plugins.utils.JSON_OBJECT_MAPPER
import org.gradle.api.Project
import org.gradle.api.artifacts.ModuleVersionIdentifier
import org.gradle.api.capabilities.MutableCapabilitiesMetadata
import kotlin.LazyThreadSafetyMode.NONE

abstract class AbstractMavenCentralComponentCapabilities(scope: String, project: Project) : AbstractComponentCapabilities(project) {

    private val capabilitiesJsonResourceName = "resolution-rules/maven-central/$scope-capabilities.json"

    private val capabilitiesMap: Map<String, List<String>> by lazy(NONE) {
        AbstractMavenCentralComponentCapabilities::class.java.classLoader
            ?.getResourceAsStream(capabilitiesJsonResourceName)
            ?.use { inputStream ->
                return@lazy JSON_OBJECT_MAPPER.readValue(
                    inputStream,
                    object : TypeReference<Map<String, List<String>>>() {}
                )
            }

        getGradleLogger(this.javaClass).warn(
            "Plugin {}: resource not found: {}",
            getPluginIdForLogging(ComponentCapabilitiesPlugin::class.java),
            capabilitiesJsonResourceName
        )
        return@lazy emptyMap<String, List<String>>()
    }

    final override fun ModuleVersionIdentifier.process(capabilities: MutableCapabilitiesMetadata) {
        capabilitiesMap["$group:$name"]?.forEach {
            capabilities.addCapability(it)
        }
    }

}
