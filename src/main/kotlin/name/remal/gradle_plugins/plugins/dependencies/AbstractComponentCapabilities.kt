package name.remal.gradle_plugins.plugins.dependencies

import name.remal.gradle_plugins.dsl.extensions.isPluginDisabledByProperty
import name.remal.gradle_plugins.dsl.utils.DependencyNotation
import name.remal.gradle_plugins.dsl.utils.parseDependencyNotation
import name.remal.nullIfEmpty
import org.gradle.api.Project
import org.gradle.api.artifacts.ComponentMetadataContext
import org.gradle.api.artifacts.ComponentMetadataDetails
import org.gradle.api.artifacts.ComponentMetadataRule
import org.gradle.api.artifacts.ModuleVersionIdentifier
import org.gradle.api.capabilities.MutableCapabilitiesMetadata

abstract class AbstractComponentCapabilities(protected val project: Project) : ComponentMetadataRule {

    companion object {
        private const val DEFAULT_VERSION = "0"
    }

    final override fun execute(context: ComponentMetadataContext) {
        if (project.isPluginDisabledByProperty(ComponentCapabilitiesPlugin::class.java)) return

        context.details.let { details ->
            details.process()
            details.allVariants {
                it.withCapabilities { capabilities ->
                    details.id.process(capabilities)
                }
            }
        }
    }


    protected open fun ComponentMetadataDetails.process() {
        // override it
    }

    protected fun ComponentMetadataDetails.belongsTo(name: String, version: String) {
        belongsTo("remal-gradle-plugins-virtual-platforms:$name:$version")
    }


    protected open fun ModuleVersionIdentifier.process(capabilities: MutableCapabilitiesMetadata) {
        // override it
    }

    protected fun MutableCapabilitiesMetadata.addCapability(group: String, name: String) = addCapability(group, "$name-capability", DEFAULT_VERSION)

    protected fun MutableCapabilitiesMetadata.addCapability(notation: DependencyNotation) = addCapability(
        notation.group, "${notation.module}-capability", notation.version.nullIfEmpty()
        ?: DEFAULT_VERSION
    )

    protected fun MutableCapabilitiesMetadata.addCapability(notation: String) = addCapability(parseDependencyNotation(notation))

}
