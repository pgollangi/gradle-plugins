package name.remal.gradle_plugins.plugins.dependencies

import name.remal.Ordered
import org.gradle.api.Project

interface DefaultDependencyVersionFactory : Ordered<DefaultDependencyVersionFactory> {
    fun create(project: Project): List<DefaultDependencyVersion>
}
