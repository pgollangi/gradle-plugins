package name.remal.gradle_plugins.plugins.dependencies

import name.remal.gradle_plugins.dsl.Extension
import org.gradle.api.Project
import org.gradle.api.artifacts.Dependency
import org.gradle.internal.jvm.Jvm
import java.io.File

@Extension
class DependencyHandlerToolsJarExtension(private val project: Project) {

    fun toolsJar(): Dependency {
        return project.dependencies.create(project.files(project.provider {
            val file = Jvm.current().toolsJar
            if (file == null) {
                return@provider emptyList<File>()
            } else {
                return@provider listOf(file)
            }
        }))
    }

}
