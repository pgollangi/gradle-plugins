package name.remal.gradle_plugins.plugins.dependencies

import name.remal.gradle_plugins.dsl.Extension
import name.remal.gradle_plugins.plugins.gradle_plugins.CrossVersionGradleLibrary.EMBEDDED_KOTLIN
import org.gradle.api.artifacts.Dependency
import org.gradle.api.artifacts.dsl.DependencyHandler

@Extension
class DependencyHandlerEmbeddedKotlinExtension(
    private val dependencies: DependencyHandler
) {

    fun embeddedKotlin(): Dependency {
        return EMBEDDED_KOTLIN.dependencyFactory(dependencies)!!
    }

}
