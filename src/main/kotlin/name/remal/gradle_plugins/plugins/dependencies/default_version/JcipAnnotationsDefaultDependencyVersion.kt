package name.remal.gradle_plugins.plugins.dependencies.default_version

import name.remal.gradle_plugins.api.AutoService
import name.remal.gradle_plugins.api.BuildTimeConstants.getStringProperty
import name.remal.gradle_plugins.plugins.dependencies.DefaultDependencyVersion

@AutoService
open class JcipAnnotationsDefaultDependencyVersion : DefaultDependencyVersion {
    override val notation: String = "net.jcip:jcip-annotations"
    override val version: String get() = getStringProperty("jcip-annotations.version")
}
