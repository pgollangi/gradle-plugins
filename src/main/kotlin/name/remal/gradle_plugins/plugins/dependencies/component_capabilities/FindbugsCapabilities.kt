package name.remal.gradle_plugins.plugins.dependencies.component_capabilities

import name.remal.gradle_plugins.api.AutoService
import name.remal.gradle_plugins.plugins.dependencies.AbstractComponentCapabilities
import org.gradle.api.Project
import org.gradle.api.artifacts.ModuleVersionIdentifier
import org.gradle.api.capabilities.MutableCapabilitiesMetadata
import javax.inject.Inject

@AutoService
class FindbugsCapabilities @Inject constructor(project: Project) : AbstractComponentCapabilities(project) {

    override fun ModuleVersionIdentifier.process(capabilities: MutableCapabilitiesMetadata) {
        if (group == "net.sourceforge.findbugs") {
            capabilities.addCapability("com.google.code.findbugs", name, version)
            return
        }
    }

}
