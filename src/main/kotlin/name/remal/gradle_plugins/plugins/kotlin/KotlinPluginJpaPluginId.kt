package name.remal.gradle_plugins.plugins.kotlin

import name.remal.gradle_plugins.dsl.PluginId

object KotlinPluginJpaPluginId : PluginId("kotlin-jpa", "org.jetbrains.kotlin.plugin.jpa")
