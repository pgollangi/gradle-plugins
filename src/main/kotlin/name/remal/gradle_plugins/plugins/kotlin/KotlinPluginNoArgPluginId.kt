package name.remal.gradle_plugins.plugins.kotlin

import name.remal.gradle_plugins.dsl.PluginId

object KotlinPluginNoArgPluginId : PluginId("kotlin-noarg", "org.jetbrains.kotlin.plugin.noarg")
