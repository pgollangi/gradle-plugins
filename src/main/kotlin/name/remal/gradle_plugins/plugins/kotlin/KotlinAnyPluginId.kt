package name.remal.gradle_plugins.plugins.kotlin

import name.remal.gradle_plugins.dsl.PluginId

object KotlinAnyPluginId : PluginId(KotlinJvmPluginId, KotlinAndroidPluginId, KotlinCommonPluginId, KotlinJsPluginId)
