package name.remal.gradle_plugins.plugins.kotlin

import name.remal.gradle_plugins.dsl.PluginId

object KotlinAndroidPluginId : PluginId("kotlin-android", "kotlin-platform-android", "org.jetbrains.kotlin.android", "org.jetbrains.kotlin.platform.android")
