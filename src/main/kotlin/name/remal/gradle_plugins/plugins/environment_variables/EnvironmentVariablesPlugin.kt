package name.remal.gradle_plugins.plugins.environment_variables

import name.remal.gradle_plugins.dsl.BaseReflectiveProjectPlugin
import name.remal.gradle_plugins.dsl.Plugin
import name.remal.gradle_plugins.dsl.PluginAction
import name.remal.gradle_plugins.dsl.reflective_project_plugin.mixin.CurrentProjectIsARootProjectMixin
import org.gradle.api.tasks.TaskContainer

@Plugin(
    id = "name.remal.environment-variables",
    description = "Plugin that creates 'environmentVariablesHelp' task.",
    tags = ["ci"]
)
class EnvironmentVariablesPlugin : BaseReflectiveProjectPlugin(), CurrentProjectIsARootProjectMixin {

    @PluginAction
    fun TaskContainer.`Create 'environmentVariablesHelp' task`() {
        create("environmentVariablesHelp", EnvironmentVariablesHelp::class.java)
    }

}
