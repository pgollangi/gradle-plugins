package name.remal.gradle_plugins.plugins.environment_variables

import name.remal.buildList
import name.remal.default
import name.remal.forEachIndexed
import name.remal.gradle_plugins.dsl.BuildTask
import name.remal.gradle_plugins.dsl.EnvironmentVariableInfo
import name.remal.gradle_plugins.dsl.EnvironmentVariableInfoFactory
import name.remal.gradle_plugins.dsl.NotDefinedEnvironmentVariableCondition
import name.remal.gradle_plugins.dsl.NotDefinedProjectPlugin
import name.remal.gradle_plugins.dsl.extensions.isPluginApplied
import name.remal.gradle_plugins.dsl.extensions.onlyIfFirstTaskWithTheNameInGraph
import name.remal.gradle_plugins.dsl.utils.findPluginId
import name.remal.loadServices
import name.remal.nullIf
import name.remal.nullIfEmpty
import org.gradle.api.DefaultTask
import org.gradle.api.plugins.HelpTasksPlugin.HELP_GROUP
import org.gradle.api.tasks.TaskAction

@BuildTask
class EnvironmentVariablesHelp : DefaultTask() {

    init {
        group = HELP_GROUP
        onlyIfFirstTaskWithTheNameInGraph()
    }

    @TaskAction
    @Suppress("ComplexMethod")
    protected fun doDisplayHelp() {
        val envVarInfos = buildList<EnvironmentVariableInfo> {
            loadServices(EnvironmentVariableInfo::class.java).forEach { add(it) }
            loadServices(EnvironmentVariableInfoFactory::class.java).forEach { addAll(it.create(project)) }
        }
            .map { it.withPluginId(it.pluginId.nullIfEmpty()).withPluginClass(it.pluginClass.nullIf { NotDefinedProjectPlugin::class.java == this }) }
            .filter {
                val pluginId = it.pluginId
                if (pluginId != null) {
                    return@filter project.isPluginApplied(pluginId)
                }
                val pluginClass = it.pluginClass
                if (pluginClass != null) {
                    return@filter project.isPluginApplied(pluginClass)
                }
                return@filter true
            }
            .map { it.withConditionClass(it.conditionClass.nullIf { NotDefinedEnvironmentVariableCondition::class.java != this }) }
            .filter {
                val condition = it.conditionClass?.getConstructor()?.newInstance() ?: return@filter true
                return@filter condition.test(project)
            }
            .map { it.withPluginId(it.pluginId ?: it.pluginClass?.let { findPluginId(it) }) }

        val pluginMappings = envVarInfos.groupBy { (it.pluginId ?: it.pluginClass?.name).default() }
            .mapValues {
                it.value.groupBy { it.scope.default() }.toSortedMap()
            }
            .toSortedMap()


        if (pluginMappings.isEmpty()) {
            logger.lifecycle("Environment variables help has no content")

        } else {
            pluginMappings.forEachIndexed { pluginIndex, pluginId, scopeMappings ->
                if (1 <= pluginIndex) logger.lifecycle("")

                if (pluginId.isNullOrEmpty()) {
                    logger.lifecycle("Common environment variables:")
                } else {
                    if (!project.isPluginApplied(pluginId)) return@forEachIndexed
                    logger.lifecycle("Environment variables for {} plugin:", pluginId)
                }

                scopeMappings.forEachIndexed { scopeIndex, scope, varInfos ->
                    if (1 <= scopeIndex) logger.lifecycle("")

                    val indent: String
                    if (scope.isNullOrEmpty()) {
                        indent = "    "
                    } else {
                        logger.lifecycle("    {}:", scope)
                        indent = "        "
                    }

                    varInfos.forEach { logger.lifecycle("{}{} - {}", indent, it.variableName, it.description) }
                }
            }
        }

        didWork = true
    }

}
