package name.remal.gradle_plugins.plugins.publish

import name.remal.gradle_plugins.dsl.PluginId

object MavenPublishPluginId : PluginId("maven-publish")
