package name.remal.gradle_plugins.plugins.publish.nexus_staging

import java.net.URI

data class NexusRepository(
    val id: String,
    val name: String,
    val resourceURI: URI,
    val provider: String
)
