package name.remal.gradle_plugins.plugins.publish.nexus_staging

import java.time.ZonedDateTime

data class NexusStagingActionEvent(
    val actionName: String,
    val actionTimestamp: ZonedDateTime,
    val event: NexusStagingRepositoryEvent
)
