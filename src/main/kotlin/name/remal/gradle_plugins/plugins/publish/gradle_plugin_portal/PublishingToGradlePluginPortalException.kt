package name.remal.gradle_plugins.plugins.publish.gradle_plugin_portal

class PublishingToGradlePluginPortalException : RuntimeException {
    constructor() : super()
    constructor(message: String?) : super(message)
    constructor(message: String?, cause: Throwable?) : super(message, cause)
    constructor(cause: Throwable?) : super(cause)
}
