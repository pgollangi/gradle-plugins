package name.remal.gradle_plugins.plugins.publish

import name.remal.gradle_plugins.dsl.BuildTask
import org.gradle.api.artifacts.repositories.ArtifactRepository

@BuildTask
class BasePublishToMavenRepository<RepositoryType : ArtifactRepository> : BasePublishToMaven() {

    lateinit var repository: RepositoryType

}
