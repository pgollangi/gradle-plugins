package name.remal.gradle_plugins.plugins.publish.nexus_staging

class NexusRepositoryTransitionException(repository: NexusStagingRepository, events: Iterable<NexusStagingActionEvent>) : RuntimeException(buildString {
    append(repository.repositoryURI)
    append(" - action failed: ")
    append(events.lastOrNull()?.actionName)
})
