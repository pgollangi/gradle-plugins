package name.remal.gradle_plugins.plugins.publish

import com.google.common.cache.CacheBuilder
import com.google.common.cache.CacheLoader
import com.google.common.cache.LoadingCache
import name.remal.gradle_plugins.dsl.BuildTask
import name.remal.setNoOpEntityResolver
import name.remal.setNoValidatingXMLReaderFactory
import org.gradle.api.publish.maven.tasks.AbstractPublishToMaven
import org.gradle.api.publish.maven.tasks.GenerateMavenPom
import org.gradle.api.publish.plugins.PublishingPlugin.PUBLISH_TASK_GROUP
import org.gradle.api.tasks.Internal
import org.jdom2.Document
import org.jdom2.Namespace
import org.jdom2.input.SAXBuilder
import java.io.File
import java.io.FileNotFoundException

@BuildTask
class BasePublishToMaven : AbstractPublishToMaven() {

    companion object {
        val POM_NAMESPACE: Namespace = Namespace.getNamespace(null, "http://maven.apache.org/POM/4.0.0")
    }

    init {
        group = PUBLISH_TASK_GROUP
    }

    @get:Internal
    protected val pomFile: File
        get() {
            val generateMavenPom = project.tasks.asSequence()
                .filterIsInstance(GenerateMavenPom::class.java)
                .filter { it.pom == publication.pom }
                .firstOrNull()
            if (generateMavenPom == null) {
                throw IllegalStateException("${GenerateMavenPom::class.java.simpleName} task can't be found for publication '${publication.name}'")
            }
            return generateMavenPom.destination.absoluteFile
        }

    @get:Internal
    protected val pomDocument: Document
        get() = pomFile.let { pomDocumentCache[it to it.lastModified()] }

    private val pomDocumentCache: LoadingCache<Pair<File, Long>, Document> = CacheBuilder.newBuilder()
        .build(object : CacheLoader<Pair<File, Long>, Document>() {
            override fun load(key: Pair<File, Long>): Document {
                val file = key.first
                val lastModified = key.second
                if (lastModified < 0) {
                    throw FileNotFoundException(file.path)
                }

                val saxBuilder = SAXBuilder().apply {
                    setNoValidatingXMLReaderFactory()
                    setNoOpEntityResolver()
                }
                return saxBuilder.build(file)
            }
        })

}
