package name.remal.gradle_plugins.plugins.ci

import name.remal.gradle_plugins.dsl.ApplyPluginClasses
import name.remal.gradle_plugins.dsl.Plugin
import name.remal.gradle_plugins.dsl.PluginAction
import name.remal.gradle_plugins.dsl.PluginActionsGroup
import name.remal.gradle_plugins.dsl.PluginCondition
import name.remal.gradle_plugins.dsl.WithPluginClasses
import name.remal.gradle_plugins.dsl.extensions.get
import name.remal.gradle_plugins.plugins.environment_variables.EnvironmentVariablesPlugin
import name.remal.gradle_plugins.plugins.vcs.VcsOperationsExtension
import name.remal.gradle_plugins.plugins.vcs.VcsOperationsPlugin
import name.remal.nullIfEmpty
import org.gradle.api.Project
import org.gradle.api.plugins.ExtensionContainer
import java.lang.System.getenv

@Plugin(
    id = "name.remal.github-actions-ci",
    description = "Plugin that helps to integrate with GitHub Actions.",
    tags = ["github", "github-actions-ci", "github-actions"]
)
@ApplyPluginClasses(EnvironmentVariablesPlugin::class)
class GitHubActionsCIPlugin : AbstractCIPlugin() {

    @PluginCondition
    fun Project.`Check if CI == 'true' && GITHUB_ACTIONS == 'true'`(): Boolean {
        return getenv("CI") == "true" && getenv("GITHUB_ACTIONS") == "true"
    }

    @PluginAction(order = -1)
    fun ExtensionContainer.`Set 'ci' extension values`() {
        this[CIExtension::class.java].run {
            isBuildOnCI = true
            pipelineId = getenv("GITHUB_RUN_ID").nullIfEmpty()
            buildId = null
            stageName = null
            jobName = null
        }
    }

    @PluginActionsGroup("Configure name.remal.vcs-operations plugin")
    @WithPluginClasses(VcsOperationsPlugin::class)
    inner class ConfigureVcsOperationsPlugin {

        @PluginAction("vcsOperations.overwriteCurrentBranch = getenv('GITHUB_REF')")
        fun ExtensionContainer.setOverwriteCurrentBranch() {
            val refName = getenv("GITHUB_REF").nullIfEmpty() ?: return

            "refs/heads/".let { prefix ->
                if (refName.startsWith(prefix)) {
                    val branch = refName.substring(prefix.length)
                    this[VcsOperationsExtension::class.java].overwriteCurrentBranch = branch
                    return
                }
            }
        }
    }

}
