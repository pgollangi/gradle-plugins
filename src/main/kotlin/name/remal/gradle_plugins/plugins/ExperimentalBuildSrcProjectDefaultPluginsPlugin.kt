package name.remal.gradle_plugins.plugins

import name.remal.gradle_plugins.dsl.ApplyPluginClasses
import name.remal.gradle_plugins.dsl.BaseReflectiveProjectPlugin
import name.remal.gradle_plugins.dsl.Plugin

@Plugin(
    id = "name.remal.experimental-buildSrc-default-plugins",
    description = "Plugin that applies buildSrc-default-plugins and name.remal.experimental-default-plugins plugins.",
    tags = ["common", "default", "buildSrc", "experimental"]
)
@ApplyPluginClasses(
    BuildSrcProjectDefaultPluginsPlugin::class,
    ExperimentalDefaultPluginsPlugin::class
)
class ExperimentalBuildSrcProjectDefaultPluginsPlugin : BaseReflectiveProjectPlugin()
