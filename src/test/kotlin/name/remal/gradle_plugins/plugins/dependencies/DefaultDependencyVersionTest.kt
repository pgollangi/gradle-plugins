package name.remal.gradle_plugins.plugins.dependencies

import org.junit.Assert.assertEquals
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Test

class DefaultDependencyVersionTest {

    @Test
    fun testComparision() {
        val obj1 = object : DefaultDependencyVersion {
            override val notation: String = "group:*"
            override val version: String = "20"
        }
        val obj2 = object : DefaultDependencyVersion {
            override val notation: String = "group:name"
            override val version: String = "15"
        }
        val obj3 = object : DefaultDependencyVersion {
            override val notation: String = "group:name"
            override val version: String = "1"
        }
        assertEquals(0, obj1.compareTo(obj1))
        assertTrue(obj2 < obj1)
        assertTrue(obj3 < obj1)
        assertTrue(obj3 < obj2)
    }

    @Test
    fun testMatchesSimple() {
        val depVer = object : DefaultDependencyVersion {
            override val notation: String = "group:name"
            override val version: String get() = TODO()
        }
        assertTrue(depVer.matches("group", "name"))
        assertFalse(depVer.matches("group", "name-subname"))
    }

    @Test
    fun testMatchesPattern() {
        run {
            val depVer = object : DefaultDependencyVersion {
                override val notation: String = "group:*"
                override val version: String get() = TODO()
            }
            assertTrue(depVer.matches("group", "name"))
            assertTrue(depVer.matches("group", "name-subname"))
            assertFalse(depVer.matches("group.subgroup", "name"))
        }
        run {
            val depVer = object : DefaultDependencyVersion {
                override val notation: String = "group.*:name"
                override val version: String get() = TODO()
            }
            assertFalse(depVer.matches("group", "name"))
            assertTrue(depVer.matches("group.subgroup", "name"))
            assertFalse(depVer.matches("group.subgroup", "name-subname"))
        }
    }

}
