package name.remal.gradle_plugins.plugins.common

import name.remal.gradle_plugins.dsl.extensions.applyPlugin
import name.remal.gradle_plugins.dsl.extensions.createWithUniqueName
import name.remal.gradle_plugins.testing.dsl.BaseProjectTest
import name.remal.uncheckedCast
import org.assertj.core.api.Assertions.assertThat
import org.gradle.api.artifacts.ModuleDependency
import org.gradle.api.file.DuplicatesStrategy.WARN
import org.gradle.api.tasks.Copy
import org.gradle.api.tasks.bundling.Jar
import org.junit.Test

class CommonSettingsPluginTest : BaseProjectTest() {

    @Test
    fun `Set duplicates strategy to WARN for copying tasks`() {
        val taskBefore = project.tasks.createWithUniqueName(Copy::class.java)
        project.applyPlugin(CommonSettingsPlugin::class.java)
        val taskAfter = project.tasks.createWithUniqueName(Copy::class.java)
        assertThat(taskBefore.duplicatesStrategy).`as`("taskBefore.duplicatesStrategy").isEqualTo(WARN)
        assertThat(taskAfter.duplicatesStrategy).`as`("taskAfter.duplicatesStrategy").isEqualTo(WARN)
    }

    @Test
    fun `Turn ON reproducible file order for archive tasks`() {
        val taskBefore = project.tasks.createWithUniqueName(Jar::class.java)
        project.applyPlugin(CommonSettingsPlugin::class.java)
        val taskAfter = project.tasks.createWithUniqueName(Jar::class.java)
        assertThat(taskBefore.isReproducibleFileOrder).`as`("taskBefore.isReproducibleFileOrder").isTrue()
        assertThat(taskAfter.isReproducibleFileOrder).`as`("taskAfter.isReproducibleFileOrder").isTrue()
    }

    @Test
    fun `Disable dependency transitivity if configuration is not transitive`() {
        val confBefore = project.configurations.createWithUniqueName { it.isTransitive = false }
        val depBefore = project.dependencies.add(confBefore.name, "name.remal:common:+")!!.uncheckedCast<ModuleDependency>()
        project.applyPlugin(CommonSettingsPlugin::class.java)
        val confAfter = project.configurations.createWithUniqueName { it.isTransitive = false }
        val depAfter = project.dependencies.add(confAfter.name, "name.remal:common:+")!!.uncheckedCast<ModuleDependency>()
        assertThat(depBefore.isTransitive).`as`("depBefore.isTransitive").isFalse()
        assertThat(depAfter.isTransitive).`as`("depAfter.isTransitive").isFalse()
    }

}
