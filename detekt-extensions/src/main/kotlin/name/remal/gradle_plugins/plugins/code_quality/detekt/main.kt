package name.remal.gradle_plugins.plugins.code_quality.detekt

import java.io.File
import java.nio.charset.StandardCharsets.UTF_8
import io.gitlab.arturbosch.detekt.cli.main as nativeMain

fun main(args: Array<String>) {
    val argsFilePath = args[0]
    val argsFile = File(argsFilePath)
    val trueArgs = argsFile.readText(UTF_8).split('\n')
    nativeMain(trueArgs.toTypedArray())
}
