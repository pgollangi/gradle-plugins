package name.remal.gradle_plugins.dsl.extensions

import name.remal.gradle_plugins.testing.dsl.BaseProjectTest
import name.remal.newTempFile
import org.gradle.testing.jacoco.tasks.JacocoReport
import org.junit.Test

class JacocoReportBaseExtensionsTest : BaseProjectTest() {

    @Test
    fun prepareExecutionData() {
        project.tasks.createWithUniqueName(JacocoReport::class.java) {
            it.prepareExecutionData()
            it.executionDataCompatible.asFileTree.visit { project.logger.debug("{}", it.relativePath) }
        }

        project.tasks.createWithUniqueName(JacocoReport::class.java) {
            it.executionDataCompatible = project.files(newTempFile("test-", ".jacoco"))
            it.prepareExecutionData()
            it.executionDataCompatible.asFileTree.visit { project.logger.debug("{}", it.relativePath) }
        }
    }

}
