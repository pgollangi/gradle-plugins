package name.remal.gradle_plugins.dsl.extensions

import name.remal.createParentDirectories
import name.remal.gradle_plugins.testing.dsl.BaseProjectTest
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNull
import org.junit.Test

class FileTreeExtensionsTest : BaseProjectTest() {

    @Test
    fun noJavaModuleInfoFile() {
        assertNull(project.emptyFileTree().getJavaModuleName())
    }

    @Test
    fun severalJavaModuleInfoFiles() {
        assertNull(project.files(
            project.projectDir.resolve("several-1").apply { resolve("module-info.java").createParentDirectories().writeText("module test {}") },
            project.projectDir.resolve("several-2").apply { resolve("module-info.java").createParentDirectories().writeText("module test {}") }
        ).asFileTree.getJavaModuleName())
    }

    @Test
    fun normalJavaModuleInfoFile() {
        assertEquals("test", project.files(
            project.projectDir.resolve("normal").apply { resolve("module-info.java").createParentDirectories().writeText("module test {}") }
        ).asFileTree.getJavaModuleName())
    }

    @Test(expected = JavaModuleNameParseException::class)
    fun javaModuleInfoFileParseError() {
        project.files(
            project.projectDir.resolve("parse-error").apply { resolve("module-info.java").createParentDirectories().writeText("module test test {}") }
        ).asFileTree.getJavaModuleName()
    }

    @Test(expected = JavaModuleDeclarationNotFoundException::class)
    fun notJavaModuleInfoFile() {
        project.files(
            project.projectDir.resolve("not-module").apply { resolve("module-info.java").createParentDirectories().writeText("class test {}") }
        ).asFileTree.getJavaModuleName()
    }

}
