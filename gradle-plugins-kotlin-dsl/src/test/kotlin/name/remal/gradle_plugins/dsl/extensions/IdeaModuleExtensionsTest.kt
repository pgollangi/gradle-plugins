package name.remal.gradle_plugins.dsl.extensions

import name.remal.gradle_plugins.testing.dsl.BaseProjectTest
import org.gradle.plugins.ide.idea.model.IdeaModel
import org.gradle.plugins.ide.idea.model.internal.GeneratedIdeaScope
import org.junit.Test

class IdeaModuleExtensionsTest : BaseProjectTest() {

    @Test
    fun addToScope() {
        project.applyPlugin("idea")
        project[IdeaModel::class.java].module!!.addToScope(GeneratedIdeaScope.TEST, project.configurations.create("addToScope"))
    }

}
