package name.remal.gradle_plugins.dsl.extensions

import name.remal.gradle_plugins.testing.dsl.BaseProjectTest
import org.gradle.api.tasks.bundling.Jar
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Test

class AbstractArchiveTaskExtensionsTest : BaseProjectTest() {

    @Test
    fun baseNameCompatible() {
        val task = project.tasks.createWithUniqueName(Jar::class.java)
        task.baseNameCompatible = "123"
        assertEquals("123", task.baseNameCompatible)
    }

    @Test
    fun archiveNameCompatible() {
        val task = project.tasks.createWithUniqueName(Jar::class.java)
        task.archiveNameCompatible = "123"
        assertEquals("123", task.archiveNameCompatible)
    }

    @Test
    fun archivePathCompatible() {
        val task = project.tasks.createWithUniqueName(Jar::class.java)
        task.destinationDirCompatible = project.buildDir
        assertTrue(task.archivePathCompatible.extension == "jar")
    }

    @Test
    fun destinationDirCompatible() {
        val task = project.tasks.createWithUniqueName(Jar::class.java)
        task.destinationDirCompatible = project.buildDir
        assertEquals(project.buildDir, task.destinationDirCompatible)
    }

    @Test
    fun extensionCompatible() {
        val task = project.tasks.createWithUniqueName(Jar::class.java)
        task.extensionCompatible = "zip"
        assertEquals("zip", task.extensionCompatible)
        assertTrue(task.archiveNameCompatible.endsWith(".zip"))
    }

    @Test
    fun classifierCompatible() {
        val task = project.tasks.createWithUniqueName(Jar::class.java)
        task.classifierCompatible = "classifier"
        assertEquals("classifier", task.classifierCompatible)
        assertTrue(task.archiveNameCompatible.contains("classifier."))
    }

}
