package name.remal.gradle_plugins.dsl.utils

import name.remal.classNameToResourceName
import name.remal.gradle_plugins.dsl.extensions.readAll
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test

class ClassDependenciesCollectorTest {

    private lateinit var collector: ClassDependenciesCollector

    @Before
    fun before() {
        collector = ClassDependenciesCollector {
            ClassDependenciesCollectorTest::class.java
                .classLoader
                .getResourceAsStream(classNameToResourceName(it))
                ?.readAll()
        }
    }

    @Test
    fun getDependenciesTest() {
        assertTrue(
            collector.getDependencies(ClassDependenciesCollectorTest::class.java.name)!!.asSequence()
                .any { it == Sequence::class.java.name }
        )
    }

    @Test
    fun getAllDependenciesTest() {
        assertTrue(
            collector.getAllDependencies(ClassDependenciesCollectorTest::class.java.name).asSequence()
                .any { it == AssertionError::class.java.name }
        )
    }

}
