package name.remal.gradle_plugins.dsl.utils

import name.remal.gradle_plugins.testing.dsl.BaseProjectTest
import org.junit.Assert.assertEquals
import org.junit.Test
import java.io.File

class NewFileCollectionDependencyTest : BaseProjectTest() {

    @Test
    fun test() {
        val dependency = newFileCollectionDependency(
            transitiveFilesFactory = { listOf(File("1"), File("2")) },
            notTransitiveFilesFactory = { listOf(File("1")) }
        )
        assertEquals(listOf(File("1"), File("2")), project.configurations.detachedConfiguration(dependency).files.sorted())
    }

}
