package name.remal.gradle_plugins.dsl.utils.code_quality

import org.junit.Assert.assertTrue
import org.junit.Test

class FindBugsReportWebJarsTest {

    val resources = FindBugsReportWebJars.resources.filterValues { it.isNotEmpty() }.keys

    @Test
    fun libOrdering() {
        val jqueryIndex = resources.indexOfFirst { it == "jquery.min.js" }
        val popperIndex = resources.indexOfFirst { it == "popper.min.js" }
        assertTrue(jqueryIndex < popperIndex)
        val bootstrapFirstIndex = resources.indexOfFirst { it == "bootstrap.min.css" || it == "bootstrap.min.js" }
        val bootstrapLastIndex = resources.indexOfLast { it == "bootstrap.min.css" || it == "bootstrap.min.js" }
        assertTrue(bootstrapFirstIndex < bootstrapLastIndex)
        assertTrue(popperIndex < bootstrapFirstIndex)
        val prismFirstIndex = resources.indexOfFirst { (!it.contains('/') && (it.startsWith("prism-") || it.startsWith("prism."))) || (it.startsWith("prism/") && it.endsWith(".js")) }
        val prismLastIndex = resources.indexOfLast { (!it.contains('/') && (it.startsWith("prism-") || it.startsWith("prism."))) || (it.startsWith("prism/") && it.endsWith(".js")) }
        assertTrue(prismFirstIndex < prismLastIndex)
        assertTrue(bootstrapLastIndex < prismFirstIndex)
    }

    @Test
    fun pathSorting() {
        val libIndex = resources.indexOf("prism.min.js")
        assertTrue(libIndex >= 0)
        val pluginIndex = resources.indexOf("prism-autolinker.min.js")
        assertTrue(pluginIndex > libIndex)
        val additionIndex = resources.indexOf("prism/java.min.js")
        assertTrue(additionIndex > pluginIndex)
    }

}
