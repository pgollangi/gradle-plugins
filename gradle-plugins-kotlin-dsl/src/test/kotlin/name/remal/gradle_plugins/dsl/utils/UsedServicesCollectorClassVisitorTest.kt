package name.remal.gradle_plugins.dsl.utils

import name.remal.accept
import name.remal.gradle_plugins.dsl.extensions.readAll
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Test
import org.objectweb.asm.ClassReader
import org.objectweb.asm.Type.getInternalName
import java.lang.ClassLoader.getSystemClassLoader
import java.util.ServiceLoader

class UsedServicesCollectorClassVisitorTest {

    @Test
    fun `load not constant`() {
        val classVisitor = UsedServicesCollectorClassVisitor()
        val classReader = ClassReader(Loader::class.java.bytecode)
        classReader.accept(classVisitor)

        assertTrue(classVisitor.usedServiceClassInternalNames.isEmpty())
    }

    private interface Service

    private class Loader {
        fun load() {
            val service = Class.forName(Service::class.java.name)
            ServiceLoader.load(service)
        }
    }


    @Test
    fun load() {
        val classVisitor = UsedServicesCollectorClassVisitor()
        val classReader = ClassReader(Loader1::class.java.bytecode)
        classReader.accept(classVisitor)

        assertEquals(
            setOf(getInternalName(Service1::class.java)),
            classVisitor.usedServiceClassInternalNames
        )
    }

    private interface Service1

    private class Loader1 {
        fun load() {
            ServiceLoader.load(Service1::class.java)
        }
    }


    @Test
    fun `load with ClassLoader`() {
        val classVisitor = UsedServicesCollectorClassVisitor()
        val classReader = ClassReader(Loader2::class.java.bytecode)
        classReader.accept(classVisitor)

        assertEquals(
            setOf(getInternalName(Service2::class.java)),
            classVisitor.usedServiceClassInternalNames
        )
    }

    private interface Service2

    private class Loader2 {
        fun load() {
            val classLoader = Loader2::class.java.classLoader ?: getSystemClassLoader()
            ServiceLoader.load(Service2::class.java, classLoader)
        }
    }


    @Test
    fun loadInstalled() {
        val classVisitor = UsedServicesCollectorClassVisitor()
        val classReader = ClassReader(Loader3::class.java.bytecode)
        classReader.accept(classVisitor)

        assertEquals(
            setOf(getInternalName(Service3::class.java)),
            classVisitor.usedServiceClassInternalNames
        )
    }

    private interface Service3

    private class Loader3 {
        fun load() {
            ServiceLoader.loadInstalled(Service3::class.java)
        }
    }

    private val Class<*>.bytecode: ByteArray get() = getResourceAsStream("${name.substringAfterLast('.')}.class")!!.readAll()

}
