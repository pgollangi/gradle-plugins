package name.remal.gradle_plugins.dsl.extensions

import name.remal.KotlinAllOpen
import name.remal.gradle_plugins.testing.dsl.BaseProjectTest
import org.junit.Assert.assertEquals
import org.junit.Test

class AnyExtensionsTest : BaseProjectTest() {

    @KotlinAllOpen
    class Extension {
        var value: String = ""
        fun getVal() = ""
    }

    @Test
    fun defaultPropertyValue() {
        val extension = project.extensions.createWithAutoName(Extension::class.java)
        extension.defaultPropertyValue(Extension::value, { "default" })
        assertEquals("default", extension.value)
        extension.value = "value"
        assertEquals("value", extension.value)
        extension.value = ""
        assertEquals("", extension.value)

        extension.defaultPropertyValue(Extension::getVal, { "default" })
        assertEquals("default", extension.getVal())
    }

}
