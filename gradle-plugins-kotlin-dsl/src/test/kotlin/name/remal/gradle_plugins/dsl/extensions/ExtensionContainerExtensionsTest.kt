package name.remal.gradle_plugins.dsl.extensions

import name.remal.gradle_plugins.testing.dsl.BaseProjectTest
import org.gradle.api.publish.PublishingExtension
import org.gradle.api.publish.maven.MavenPublication
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Test

class ExtensionContainerExtensionsTest : BaseProjectTest() {

    @Test
    fun invoke() {
        project.applyPlugin("java")
        project.applyPlugin("maven-publish")

        project.invoke(PublishingExtension::class.java) {
            it.publications.create("1", MavenPublication::class.java)
        }

        val publications = project[PublishingExtension::class.java].publications
        assertEquals(1, publications.size)
        assertTrue("1" in publications)

        publications.create("2", MavenPublication::class.java)
        assertEquals(2, publications.size)
        assertTrue("1" in publications)
        assertTrue("2" in publications)

        project.invoke(PublishingExtension::class.java) {
            it.publications.create("3", MavenPublication::class.java)
        }
        assertEquals(3, publications.size)
        assertTrue("1" in publications)
        assertTrue("2" in publications)
        assertTrue("3" in publications)
    }

}
