package name.remal.gradle_plugins.dsl.utils

import org.junit.Assert.assertEquals
import org.junit.Assert.assertNull
import org.junit.Test

class CanonizeTextTest {

    @Test
    fun `null`() {
        assertNull(canonizeText(null))
    }

    @Test
    fun `blank`() {
        assertNull(canonizeText("\n\t\r"))
    }

    @Test
    fun `r`() {
        assertEquals("g\ng\ng\ng\n\ng\n\ng", canonizeText("g\rg\r\ng\n\rg\r\n\rg\n\r\rg"))
    }

    @Test
    fun `t`() {
        assertEquals("1    2", canonizeText("\t1\t2\t"))
    }

    @Test
    fun `trim n`() {
        assertEquals("1\n2", canonizeText("\n\n1\n2\n\n"))
    }

    @Test
    fun `lines - trim end`() {
        assertEquals("1\n\n2", canonizeText("1\n  \n2\t"))
    }

    @Test
    fun `lines - indent`() {
        assertEquals("1\n\n  2", canonizeText("  1\n  \n    2\t"))
    }

    @Test
    fun `several n`() {
        assertEquals("  1\n\n2", canonizeText("  1\n  \n  \n2"))
    }

}
