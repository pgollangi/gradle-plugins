package name.remal.gradle_plugins.dsl.extensions

import org.gradle.api.file.RelativePath
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Test

class RelativePathExtensionsTest {

    @Test
    fun startsWith() {
        assertTrue(RelativePath(true, "1", "2").startsWith(RelativePath(true, "1", "2")))
        assertTrue(RelativePath(true, "1", "2").startsWith(RelativePath(false, "1")))
        assertFalse(RelativePath(true, "1", "2").startsWith(RelativePath(false, "1", "2")))
        assertFalse(RelativePath(true, "1", "2").startsWith(RelativePath(true, "1", "2", "3")))
        assertFalse(RelativePath(true, "1", "2").startsWith(RelativePath(true, "1")))
        assertFalse(RelativePath(true, "1", "2").startsWith(RelativePath(true, "2")))
        assertFalse(RelativePath(true, "1", "2").startsWith(RelativePath(false, "2")))

        assertTrue(RelativePath(false, "1", "2").startsWith(RelativePath(false, "1", "2")))
        assertTrue(RelativePath(false, "1", "2").startsWith(RelativePath(false, "1")))
        assertFalse(RelativePath(false, "1", "2").startsWith(RelativePath(true, "1", "2")))
        assertFalse(RelativePath(false, "1", "2").startsWith(RelativePath(false, "1", "2", "3")))
        assertFalse(RelativePath(false, "1", "2").startsWith(RelativePath(true, "1", "2")))
        assertFalse(RelativePath(false, "1", "2").startsWith(RelativePath(true, "1")))
        assertFalse(RelativePath(false, "1", "2").startsWith(RelativePath(true, "2")))
    }

    @Test
    fun endsWith() {
        assertTrue(RelativePath(true, "1", "2").endsWith(RelativePath(true, "1", "2")))
        assertTrue(RelativePath(true, "1", "2", "3").endsWith(RelativePath(true, "2", "3")))
        assertTrue(RelativePath(false, "1", "2").endsWith(RelativePath(false, "1", "2")))
        assertTrue(RelativePath(false, "1", "2", "3").endsWith(RelativePath(false, "2", "3")))

        assertFalse(RelativePath(false, "1", "2").endsWith(RelativePath(true, "1", "2")))
        assertFalse(RelativePath(true, "1", "2", "3").endsWith(RelativePath(false, "2", "3")))
        assertFalse(RelativePath(true, "1", "2").endsWith(RelativePath(false, "1", "2")))
        assertFalse(RelativePath(false, "1", "2", "3").endsWith(RelativePath(true, "2", "3")))
    }

}
