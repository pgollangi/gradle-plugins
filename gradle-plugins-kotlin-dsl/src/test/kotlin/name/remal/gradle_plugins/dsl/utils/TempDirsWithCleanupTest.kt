package name.remal.gradle_plugins.dsl.utils

import name.remal.newTempDir
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Test
import java.io.FileFilter

class TempDirsWithCleanupTest {

    val prefix = TempDirsWithCleanupTest::class.java.simpleName + '-'

    val baseTmpDir = newTempDir()

    val tempDirsWithCleanup = TempDirsWithCleanup(
        prefix,
        baseTmpDir = baseTmpDir,
        cleanupDelayMilliseconds = 0,
        autoCleanupPeriodMilliseconds = 0
    )


    @After
    fun after() {
        baseTmpDir.deleteRecursively()
    }


    @Test
    fun test() {
        tempDirsWithCleanup.withTempDir { dir ->
            assertTrue(dir.isDirectory)
            Thread.sleep(10);
            dir.resolve("file").createNewFile()
            assertTrue(dir.resolve("file").isFile)
        }
        tempDirsWithCleanup.cleanup()
        assertEquals(0, baseTmpDir.listFiles(FileFilter { it.name.startsWith(prefix) })?.size ?: 0)
    }

}
