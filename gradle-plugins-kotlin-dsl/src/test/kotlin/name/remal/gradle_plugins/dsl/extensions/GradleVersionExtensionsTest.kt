package name.remal.gradle_plugins.dsl.extensions

import org.gradle.util.GradleVersion.version
import org.junit.Assert.assertEquals
import org.junit.Test

class GradleVersionExtensionsTest {

    @Test
    fun minorVersion() {
        assertEquals(
            version("1.2"),
            version("1.2").minorVersion
        )
        assertEquals(
            version("1.2"),
            version("1.2-rc-1").minorVersion
        )
        assertEquals(
            version("1.2"),
            version("1.2.3").minorVersion
        )
        assertEquals(
            version("1.2"),
            version("1.2.3-rc-1").minorVersion
        )
    }

}
