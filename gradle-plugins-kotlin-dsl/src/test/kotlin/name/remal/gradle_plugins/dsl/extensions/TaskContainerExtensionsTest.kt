package name.remal.gradle_plugins.dsl.extensions

import name.remal.gradle_plugins.testing.dsl.BaseProjectTest
import org.gradle.api.tasks.Copy
import org.junit.Assert.assertEquals
import org.junit.Assert.assertFalse
import org.junit.Assert.assertNotEquals
import org.junit.Assert.assertNotNull
import org.junit.Assert.assertTrue
import org.junit.Test

class TaskContainerExtensionsTest : BaseProjectTest() {

    @Test
    fun registerString() {
        val name = this::registerString.name
        project.tasks.registerCompatible(name)
        assertNotNull(project.tasks[name])
        assertFalse(Copy::class.java.isInstance(project.tasks[name]))
        assertNotEquals(TaskContainerExtensionsTest::class.java.name, project.tasks[name].description)
    }

    @Test
    fun registerStringClass() {
        val name = this::registerStringClass.name
        project.tasks.registerCompatible(name, Copy::class.java)
        assertNotNull(project.tasks[name])
        assertTrue(Copy::class.java.isInstance(project.tasks[name]))
        assertNotEquals(TaskContainerExtensionsTest::class.java.name, project.tasks[name].description)
    }

    @Test
    fun registerStringAction() {
        val name = this::registerStringAction.name
        project.tasks.registerCompatible(name, { it.description = TaskContainerExtensionsTest::class.java.name })
        assertNotNull(project.tasks[name])
        assertFalse(Copy::class.java.isInstance(project.tasks[name]))
        assertEquals(TaskContainerExtensionsTest::class.java.name, project.tasks[name].description)
    }

    @Test
    fun registerStringClassAction() {
        val name = this::registerStringClassAction.name
        project.tasks.registerCompatible(name, Copy::class.java, { it.description = TaskContainerExtensionsTest::class.java.name })
        assertNotNull(project.tasks[name])
        assertTrue(Copy::class.java.isInstance(project.tasks[name]))
        assertEquals(TaskContainerExtensionsTest::class.java.name, project.tasks[name].description)
    }

}
