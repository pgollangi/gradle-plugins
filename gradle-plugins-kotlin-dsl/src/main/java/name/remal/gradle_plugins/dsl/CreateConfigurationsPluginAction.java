package name.remal.gradle_plugins.dsl;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import name.remal.annotation.AnnotationAttributeAlias;

@PluginAction(order = Integer.MIN_VALUE + 200)
@Documented
@Inherited
@Target({METHOD, ANNOTATION_TYPE})
@Retention(RUNTIME)
public @interface CreateConfigurationsPluginAction {

    @AnnotationAttributeAlias(annotationClass = PluginAction.class, attributeName = "value")
    String value() default "";

    @AnnotationAttributeAlias(annotationClass = PluginAction.class, attributeName = "isHidden")
    boolean isHidden() default false;

}
