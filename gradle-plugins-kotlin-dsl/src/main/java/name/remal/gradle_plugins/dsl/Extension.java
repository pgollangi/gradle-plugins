package name.remal.gradle_plugins.dsl;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import name.remal.KotlinAllOpen;

@Documented
@Target(TYPE)
@Retention(RUNTIME)
@KotlinAllOpen
public @interface Extension {

    String value() default "";

}
