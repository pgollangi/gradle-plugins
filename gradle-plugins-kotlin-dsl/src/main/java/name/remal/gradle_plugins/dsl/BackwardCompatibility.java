package name.remal.gradle_plugins.dsl;

import static java.lang.annotation.RetentionPolicy.CLASS;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;

@Retention(CLASS)
@Documented
public @interface BackwardCompatibility {

    GradleEnumVersion value();

}
