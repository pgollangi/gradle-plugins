package name.remal.gradle_plugins.dsl;

import static java.lang.annotation.ElementType.CONSTRUCTOR;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.CLASS;

import java.lang.annotation.Documented;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import org.gradle.api.Project;

@Documented
@Repeatable(EnvironmentVariable.EnvironmentVariables.class)
@Target({TYPE, FIELD, METHOD, PARAMETER, CONSTRUCTOR})
@Retention(CLASS)
public @interface EnvironmentVariable {

    String value();

    String description() default "";

    String pluginId() default "";

    Class<? extends org.gradle.api.Plugin<? extends Project>> pluginClass() default NotDefinedProjectPlugin.class;

    String scope() default "";

    Class<? extends EnvironmentVariableCondition> conditionClass() default NotDefinedEnvironmentVariableCondition.class;

    @Documented
    @Target({TYPE, FIELD, METHOD, PARAMETER, CONSTRUCTOR})
    @Retention(CLASS)
    @interface EnvironmentVariables {

        EnvironmentVariable[] value();

        String description() default "";

        String pluginId() default "";

        Class<? extends org.gradle.api.Plugin<? extends Project>> pluginClass() default NotDefinedProjectPlugin.class;

        String scope() default "";

        Class<? extends EnvironmentVariableCondition> conditionClass() default NotDefinedEnvironmentVariableCondition.class;

    }

}
