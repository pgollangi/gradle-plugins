package name.remal.gradle_plugins.dsl;

import java.util.List;
import org.gradle.api.Project;

public interface EnvironmentVariableInfoFactory {

    List<EnvironmentVariableInfo> create(Project project);

}
