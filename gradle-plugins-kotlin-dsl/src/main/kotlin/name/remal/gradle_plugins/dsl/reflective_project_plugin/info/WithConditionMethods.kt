package name.remal.gradle_plugins.dsl.reflective_project_plugin.info

interface WithConditionMethods {
    val conditionMethods: List<ConditionMethodInfo>
}
