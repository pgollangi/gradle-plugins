package name.remal.gradle_plugins.dsl.extensions

import name.remal.findDeclaredField
import name.remal.gradle_plugins.dsl.utils.getGradleLoggerForCurrentClass
import name.remal.hierarchy
import name.remal.uncheckedCast
import org.gradle.api.tasks.TaskInputs

operator fun TaskInputs.set(propertyName: String, propertyValue: Any?) {
    property(propertyName, propertyValue)
}


private const val REGISTERED_FILE_PROPERTIES_FIELD_NAME = "registeredFileProperties"

fun TaskInputs.clearRegisteredFileProperties() {
    var isFieldFound = false
    javaClass.unwrapGradleGenerated().hierarchy.forEach { clazz ->
        val field = clazz.findDeclaredField(REGISTERED_FILE_PROPERTIES_FIELD_NAME) ?: return@forEach
        isFieldFound = true
        val values = field.makeAccessible().get(this).uncheckedCast<MutableIterable<*>>().iterator()
        while (values.hasNext()) {
            values.next()
            values.remove()
        }
    }

    if (!isFieldFound) {
        fileLogger.error("Field can't be found in class hierarchy: {}", REGISTERED_FILE_PROPERTIES_FIELD_NAME)
    }
}


private val fileLogger = getGradleLoggerForCurrentClass()
