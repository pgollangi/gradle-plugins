package name.remal.gradle_plugins.dsl.utils

import name.remal.concurrentMapOf
import name.remal.getMetaAnnotation
import name.remal.gradle_plugins.dsl.IMPLEMENTATION_CLASS_PLUGIN_DESCRIPTOR_KEY
import name.remal.gradle_plugins.dsl.Plugin
import name.remal.gradle_plugins.dsl.ProjectPluginClass
import name.remal.gradle_plugins.dsl.extensions.unwrapGradleGenerated
import name.remal.nullIfEmpty
import java.util.Comparator.comparingInt
import java.util.Comparator.naturalOrder
import java.util.Properties
import java.util.concurrent.ConcurrentMap

private val pluginIdCache: ConcurrentMap<ProjectPluginClass, String> = concurrentMapOf()

fun findPluginId(pluginClass: ProjectPluginClass): String? {
    return pluginIdCache.computeIfAbsent(pluginClass.unwrapGradleGenerated()) { pluginClassKey ->
        run {
            var currentClass: Class<*>? = pluginClassKey
            while (currentClass != null && Any::class.java != currentClass) {
                currentClass.getMetaAnnotation(Plugin::class.java)?.id?.nullIfEmpty()?.let {
                    return@computeIfAbsent it
                }
                currentClass = currentClass.superclass
            }
        }

        val pluginIds = retrieveAllPluginIds(pluginClassKey.classLoader).asSequence()
            .filter { pluginId ->
                val properties = Properties().also { props ->
                    try {
                        pluginClassKey.getResourceAsStream("/META-INF/gradle-plugins/$pluginId.properties").use {
                            props.load(it)
                        }
                    } catch (e: Exception) {
                        return@filter false
                    }
                }
                val implementationClass = properties.getProperty(IMPLEMENTATION_CLASS_PLUGIN_DESCRIPTOR_KEY)
                return@filter implementationClass == pluginClassKey.name
            }
            .toSortedSet(comparingInt(String::length).reversed().then(naturalOrder()))

        return@computeIfAbsent pluginIds.firstOrNull() ?: ""
    }.nullIfEmpty()
}

fun getPluginIdForLogging(pluginClass: ProjectPluginClass): String = findPluginId(pluginClass)
    ?: pluginClass.unwrapGradleGenerated().name
