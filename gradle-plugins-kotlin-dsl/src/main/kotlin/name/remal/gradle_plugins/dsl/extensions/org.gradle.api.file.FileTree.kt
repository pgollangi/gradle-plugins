@file:Suppress("UNNECESSARY_NOT_NULL_ASSERTION")

package name.remal.gradle_plugins.dsl.extensions

import com.github.javaparser.JavaParser
import com.github.javaparser.ParseProblemException
import com.github.javaparser.ParserConfiguration
import com.github.javaparser.ParserConfiguration.LanguageLevel.RAW
import name.remal.gradle_plugins.dsl.utils.getGradleLoggerForCurrentClass
import org.gradle.api.file.EmptyFileVisitor
import org.gradle.api.file.FileTree
import org.gradle.api.file.FileTreeElement
import org.gradle.api.file.FileVisitDetails
import org.gradle.api.file.FileVisitor
import org.gradle.api.tasks.util.PatternSet
import java.nio.charset.Charset
import java.nio.charset.StandardCharsets.UTF_8

fun FileTree.exclude(vararg excludes: String) = matching(PatternSet().exclude(*excludes))!!
fun FileTree.exclude(excludes: Iterable<String>) = matching(PatternSet().exclude(excludes))!!
fun FileTree.exclude(excludeSpec: (element: FileTreeElement) -> Boolean) = matching(PatternSet().exclude(excludeSpec))!!

fun FileTree.include(vararg includes: String) = matching(PatternSet().include(*includes))!!
fun FileTree.include(includes: Iterable<String>) = matching(PatternSet().include(includes))!!
fun FileTree.include(includeSpec: (element: FileTreeElement) -> Boolean) = matching(PatternSet().include(includeSpec))!!


fun FileTree.visitFiles(visitor: FileVisitor) = visit(object : EmptyFileVisitor() {
    override fun visitFile(fileDetails: FileVisitDetails) {
        visitor.visitFile(fileDetails)
    }
})!!

fun FileTree.visitFiles(visitor: (fileDetails: FileVisitDetails) -> Unit) = visit(object : EmptyFileVisitor() {
    override fun visitFile(fileDetails: FileVisitDetails) {
        visitor(fileDetails)
    }
})!!


fun FileTree.visitDirectories(visitor: FileVisitor) = visit(object : EmptyFileVisitor() {
    override fun visitDir(dirDetails: FileVisitDetails) {
        visitor.visitDir(dirDetails)
    }
})!!

fun FileTree.visitDirectories(visitor: (dirDetails: FileVisitDetails) -> Unit) = visit(object : EmptyFileVisitor() {
    override fun visitDir(fileDetails: FileVisitDetails) {
        visitor(fileDetails)
    }
})!!


fun FileTree.getJavaModuleName(sourcesCharset: Charset = UTF_8): String? {
    val moduleInfoFiles = include("module-info.java").files
    if (moduleInfoFiles.isEmpty()) return null
    if (moduleInfoFiles.size >= 2) {
        fileLogger.warn("File tree has more than one 'module-info.java' files: {}", moduleInfoFiles.joinToString(", "))
        return null
    }

    val parserConfiguration = ParserConfiguration().apply {
        languageLevel = RAW
        characterEncoding = sourcesCharset
    }
    val javaModuleParser = JavaParser(parserConfiguration)
    val moduleInfoFile = moduleInfoFiles.single()
    val parseResult = javaModuleParser.parse(moduleInfoFile)
    if (!parseResult.isSuccessful) {
        throw JavaModuleNameParseException("$moduleInfoFile: parsing failed", ParseProblemException(parseResult.problems))
    }
    val compilationUnit = parseResult.result.get()
    return compilationUnit.module.orElseThrow { JavaModuleDeclarationNotFoundException("$moduleInfoFile: not a Java module file") }.nameAsString
}

class JavaModuleNameParseException : RuntimeException {
    constructor() : super()
    constructor(message: String?) : super(message)
    constructor(message: String?, cause: Throwable?) : super(message, cause)
    constructor(cause: Throwable?) : super(cause)
}

class JavaModuleDeclarationNotFoundException : RuntimeException {
    constructor() : super()
    constructor(message: String?) : super(message)
    constructor(message: String?, cause: Throwable?) : super(message, cause)
    constructor(cause: Throwable?) : super(cause)
}


private val fileLogger = getGradleLoggerForCurrentClass()
