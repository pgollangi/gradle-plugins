package name.remal.gradle_plugins.dsl.extensions

import org.gradle.api.publish.Publication

val Publication.signTaskName: String get() = "sign${name.capitalize()}Publication"
