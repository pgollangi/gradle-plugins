package name.remal.gradle_plugins.dsl.extensions

import name.remal.gradle_plugins.dsl.utils.DependencyNotation
import name.remal.gradle_plugins.dsl.utils.DependencyNotationMatcher
import org.gradle.api.artifacts.ModuleVersionIdentifier

val ModuleVersionIdentifier.notation
    get() = DependencyNotation(
        group = group,
        module = name,
        version = version
    )


fun DependencyNotationMatcher.matches(selector: ModuleVersionIdentifier) = matches(selector.notation)
fun DependencyNotationMatcher.notMatches(selector: ModuleVersionIdentifier) = !matches(selector)
