package name.remal.gradle_plugins.dsl.extensions

import name.remal.isPublic
import java.lang.reflect.Field

@Suppress("DEPRECATION")
fun Field.makeAccessible() = apply {
    if (isAccessible) return@apply
    if (!isPublic || !declaringClass.isPublic) {
        isAccessible = true
    }
}
