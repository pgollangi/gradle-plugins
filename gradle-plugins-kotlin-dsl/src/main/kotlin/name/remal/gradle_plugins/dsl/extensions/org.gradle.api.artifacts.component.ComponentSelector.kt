@file:Suppress("UNNECESSARY_SAFE_CALL")

package name.remal.gradle_plugins.dsl.extensions

import name.remal.default
import org.gradle.api.Project
import org.gradle.api.artifacts.component.ComponentSelector
import org.gradle.api.artifacts.component.ModuleComponentSelector
import org.gradle.api.artifacts.component.ProjectComponentSelector

fun ComponentSelector.calculateGroup(project: Project): String? = when (this) {
    is ModuleComponentSelector -> this.group.default()
    is ProjectComponentSelector -> project.evaluationDependsOn(this.projectPath).group?.toString().default()
    else -> null
}

fun ComponentSelector.calculateModule(project: Project): String? = when (this) {
    is ModuleComponentSelector -> this.module.default()
    is ProjectComponentSelector -> project.evaluationDependsOn(this.projectPath).name?.toString().default()
    else -> null
}

fun ComponentSelector.calculateVersion(project: Project): String? = when (this) {
    is ModuleComponentSelector -> this.version.default()
    is ProjectComponentSelector -> project.evaluationDependsOn(this.projectPath).version?.toString().default()
    else -> null
}
