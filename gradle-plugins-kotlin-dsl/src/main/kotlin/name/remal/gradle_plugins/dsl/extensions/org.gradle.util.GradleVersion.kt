package name.remal.gradle_plugins.dsl.extensions

import name.remal.gradle_plugins.dsl.GradleEnumVersion
import org.gradle.util.GradleVersion

operator fun GradleVersion.compareTo(version: String) = compareTo(GradleVersion.version(version))
operator fun String.compareTo(version: GradleVersion) = GradleVersion.version(this).compareTo(version)

operator fun GradleVersion.compareTo(version: GradleEnumVersion) = compareTo(version.native)
operator fun GradleEnumVersion.compareTo(version: GradleVersion) = native.compareTo(version)


val GradleVersion.minorVersion: GradleVersion
    get() {
        val baseVersion = baseVersion
        val tokens = baseVersion.version.split('.')
        if (tokens.size <= 2) {
            return baseVersion
        } else {
            val minorVersionsString = tokens.take(2).joinToString(".")
            return GradleVersion.version(minorVersionsString)
        }
    }

