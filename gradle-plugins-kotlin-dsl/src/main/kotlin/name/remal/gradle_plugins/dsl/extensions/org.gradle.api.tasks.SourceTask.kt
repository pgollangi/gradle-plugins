package name.remal.gradle_plugins.dsl.extensions

import name.remal.buildSet
import org.gradle.api.tasks.SourceSet
import org.gradle.api.tasks.SourceTask
import java.io.File

val SourceTask.sourceDirs: Set<File>
    get() = buildSet {
        source.visit { details ->
            var file = details.file.absoluteFile
            repeat(details.relativePath.segments.size) {
                file = file.parentFile ?: return@visit
            }
            add(file)
        }
    }


fun SourceTask.isProcessingSourceSet(sourceSet: SourceSet): Boolean {
    val sourceSetFiles = sourceSet.allSource.files
    return source.files.any { it in sourceSetFiles }
}
