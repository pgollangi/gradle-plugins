package name.remal.gradle_plugins.dsl.extensions

import org.gradle.api.NamedDomainObjectContainer

fun <T> NamedDomainObjectContainer<T>.getOrCreate(name: String, onCreate: (T) -> Unit = {}): T {
    findByName(name)?.let { return it }
    return create(name, onCreate)
}

fun <T> NamedDomainObjectContainer<T>.createWithUniqueName(namePrefix: String = "", configurer: (T) -> Unit = {}): T {
    lateinit var name: String
    run {
        var num = 0
        while (true) {
            ++num
            name = "$namePrefix\$$num"
            if (null == this.findByName(name)) break
        }
    }
    return create(name, configurer)
}

fun <T> NamedDomainObjectContainer<T>.createWithOptionalUniqueSuffix(name: String, configurer: (T) -> Unit = {}): T {
    if (name !in this) {
        return create(name, configurer)
    }

    var num = 0
    while (true) {
        ++num
        val nameWithSuffix = "$name\$$num"
        if (null == this.findByName(name)) {
            return create(nameWithSuffix, configurer)
        }
    }
}

inline fun <T, R> NamedDomainObjectContainer<T>.forTemp(namePrefix: String = "temp", action: (T) -> R): R {
    val obj = createWithUniqueName(namePrefix)
    try {
        return action(obj)

    } finally {
        remove(obj)
    }
}


