package name.remal.gradle_plugins.dsl.reflective_project_plugin.action_param_injector

import name.remal.gradle_plugins.api.AutoService
import name.remal.gradle_plugins.dsl.extensions.ext
import org.gradle.api.Project
import org.gradle.api.plugins.ExtraPropertiesExtension

@AutoService
class ExtraPropertiesExtensionActionParamInjector : ActionParamInjector<ExtraPropertiesExtension>() {
    override fun createValue(project: Project): ExtraPropertiesExtension = project.ext
}
