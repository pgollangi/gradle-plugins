package name.remal.gradle_plugins.dsl.utils.code_quality

import name.remal.createParentDirectories
import name.remal.forceDeleteRecursively
import name.remal.gradle_plugins.dsl.utils.XML_PRETTY_OUTPUTTER
import name.remal.setAttribute
import org.jdom2.Document
import org.jdom2.Element
import java.io.File
import java.io.OutputStream
import java.io.Writer

fun FindBugsReport.writeXmlTo(outputStream: OutputStream) = XML_PRETTY_OUTPUTTER.output(toXmlDocument(), outputStream)
fun FindBugsReport.writeXmlTo(writer: Writer) = XML_PRETTY_OUTPUTTER.output(toXmlDocument(), writer)
fun FindBugsReport.writeXmlTo(file: File) = file.forceDeleteRecursively().createParentDirectories().outputStream().use { XML_PRETTY_OUTPUTTER.output(toXmlDocument(), it) }

fun FindBugsReport.asXmlString(): String = XML_PRETTY_OUTPUTTER.outputString(toXmlDocument())

@Suppress("ComplexMethod")
fun FindBugsReport.toXmlDocument() = Document().apply {
    rootElement = Element("BugCollection").apply {
        tool?.let { setAttribute("tool", it) }
        version?.let { setAttribute("version", it) }
        analysisTimestamp?.let {
            setAttribute("analysisTimestamp", it)
            setAttribute("timestamp", it)
        }

        project?.let { project ->
            addContent(Element("Project").apply {
                project.name?.let { setAttribute("projectName", it) }

                project.srcDirs.forEach { srcDir ->
                    addContent(Element("SrcDir").setText(srcDir))
                }
            })
        }

        bugs.forEach { bug ->
            addContent(Element("BugInstance").apply {
                bug.type?.let { setAttribute("type", it) }
                bug.category?.let { setAttribute("category", it) }
                bug.rank?.let { setAttribute("rank", it) }
                bug.confidence?.let { setAttribute("priority", it) }

                bug.message?.let { addContent(Element("LongMessage").setText(it)) }
                bug.shortMessage?.let { addContent(Element("ShortMessage").setText(it)) }

                bug.location?.let { location ->
                    addContent(Element("SourceLine").apply {
                        location.className?.let { setAttribute("classname", it) }
                        location.sourceFile?.let {
                            setAttribute("sourcepath", it)
                            setAttribute("sourcefile", it)
                        }
                        location.startLine?.let { setAttribute("start", it) }
                        location.startLineOffset?.let { setAttribute("startOffset", it) }
                        location.endLine?.let { setAttribute("end", it) }
                        location.endLineOffset?.let { setAttribute("endOffset", it) }
                    })
                }
            })
        }

        types.forEach { typeName, typeInfo ->
            addContent(Element("BugPattern").apply {
                setAttribute("type", typeName)
                addContent(Element("Details").setText(typeInfo.htmlDescription))
            })
        }

        categories.forEach { categoryName, categoryInfo ->
            addContent(Element("BugCategory").apply {
                setAttribute("category", categoryName)
                addContent(Element("Description").setText(categoryInfo.htmlDescription))
            })
        }
    }
}
