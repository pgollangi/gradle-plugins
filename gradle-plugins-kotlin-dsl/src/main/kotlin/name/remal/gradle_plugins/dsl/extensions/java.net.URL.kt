package name.remal.gradle_plugins.dsl.extensions

import name.remal.gradle_plugins.dsl.DEFAULT_IO_TIMEOUT
import java.net.HttpURLConnection
import java.net.URL

fun URL.fetchHttpStatus(): Int {
    val connection = openConnection()
    connection as HttpURLConnection
    connection.connectTimeout = DEFAULT_IO_TIMEOUT.toMillis().toInt()
    connection.readTimeout = DEFAULT_IO_TIMEOUT.toMillis().toInt()
    connection.useCaches = false
    connection.instanceFollowRedirects = true
    connection.requestMethod = "HEAD"
    connection.connect()
    try {
        return connection.responseCode

    } finally {
        connection.disconnect()
    }
}
