package name.remal.gradle_plugins.dsl.utils

typealias ClassName = String
typealias ClassInternalName = String


fun classNameToClassInternalName(className: ClassName): ClassInternalName = className.replace('.', '/')

fun classInternalNameToClassName(classInternalName: ClassInternalName): ClassName = classInternalName.replace('/', '.')
