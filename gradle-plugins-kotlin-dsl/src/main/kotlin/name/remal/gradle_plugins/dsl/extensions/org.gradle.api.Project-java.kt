package name.remal.gradle_plugins.dsl.extensions

import org.gradle.api.Project
import org.gradle.api.plugins.JavaPluginConvention


private val javaPackageNameProhibitedChars = Regex("[^.\\w]")
private val javaPackageNameProhibitedCharsAfterDot = Regex("\\.([^A-Za-z_])")
val Project.javaPackageName: String
    get() {
        var result = this.id
        result = result.replace(javaPackageNameProhibitedChars, "_")
        result = result.replace(javaPackageNameProhibitedCharsAfterDot, "._$1")
        return result
    }

val Project.javaModuleName: String get() = javaPackageName


val Project.java: JavaPluginConvention get() = this[JavaPluginConvention::class.java]
