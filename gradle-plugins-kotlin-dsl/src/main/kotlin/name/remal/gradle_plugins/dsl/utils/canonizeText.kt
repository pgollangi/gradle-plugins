package name.remal.gradle_plugins.dsl.utils

private val newLineRegex = Regex("(?:\\r\\n|\\n\\r|\\r)")
private val severalNewLinesRegex = Regex("\n{3,}")

fun canonizeText(value: String?, tabSize: Int = 4): String? {
    if (value == null) return null

    var result = value.replace(newLineRegex, "\n")
    result = result.replace("\t", " ".repeat(tabSize))
    result = result.trim('\n')
    if (result.isEmpty()) return null

    result = result.splitToSequence('\n')
        .map(String::trimEnd)
        .toMutableList()
        .also { lines ->
            var minIndent = -1
            lines.forEach { line ->
                if (line.isNotEmpty()) {
                    if (minIndent < 0) {
                        minIndent = line.indexOfFirst { it != ' ' }
                    } else {
                        minIndent = Math.min(minIndent, line.indexOfFirst { it != ' ' })
                    }
                }
            }

            if (minIndent > 0) {
                lines.forEachIndexed { index, line ->
                    if (line.isNotEmpty()) {
                        lines[index] = line.substring(minIndent)
                    }
                }
            }
        }
        .joinToString("\n")
    if (result.isEmpty()) return null

    return result.replace(severalNewLinesRegex, "\n\n")
}

