package name.remal.gradle_plugins.dsl.extensions

import groovy.lang.Closure
import org.gradle.api.Action
import org.gradle.api.specs.Spec
import org.gradle.api.specs.Specs.convertClosureToSpec
import org.gradle.util.ConfigureUtil.configure

fun Closure<*>.toRunnable(): Runnable = Runnable { this.call() }

fun <T> Closure<*>.toConfigureAction(): Action<T> = Action { configure(this, it) }
fun <T> Closure<*>.toConfigureKotlinFunction() = { arg: T -> configure(this, arg); Unit }

fun <T> Closure<*>.toSpec(): Spec<T> = convertClosureToSpec(this)
