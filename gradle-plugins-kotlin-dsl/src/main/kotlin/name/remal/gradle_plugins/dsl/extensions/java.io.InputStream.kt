package name.remal.gradle_plugins.dsl.extensions

import java.io.ByteArrayOutputStream
import java.io.InputStream

fun InputStream.readAll(): ByteArray = use {
    val out = ByteArrayOutputStream()
    it.copyTo(out, 8192)
    return out.toByteArray()
}
