package name.remal.gradle_plugins.dsl.utils

import name.remal.default
import name.remal.fromUpperCamelToLowerHyphen
import name.remal.gradle_plugins.dsl.extensions.newDelegateOf
import org.gradle.api.artifacts.Dependency
import org.gradle.api.artifacts.FileCollectionDependency
import org.gradle.api.artifacts.component.ComponentIdentifier
import org.gradle.api.file.FileCollection
import org.gradle.api.internal.artifacts.dependencies.SelfResolvingDependencyInternal
import org.gradle.api.tasks.TaskDependency
import java.io.File

fun newFileCollectionDependency(
    transitiveFilesFactory: () -> Iterable<File>,
    notTransitiveFilesFactory: () -> Iterable<File> = transitiveFilesFactory,
    group: String? = null,
    name: String? = null,
    version: String? = null,
    taskDependency: TaskDependency? = null
): FileCollectionDependency {
    val impl = FileCollectionDependencyImpl(
        transitiveFilesFactory = transitiveFilesFactory,
        notTransitiveFilesFactory = notTransitiveFilesFactory,
        groupValue = group,
        nameValue = name ?: DEFAULT_NAME,
        versionValue = version,
        taskDependency = taskDependency ?: EmptyTaskDependencies
    )
    return FileCollectionDependencyInterface::class.java.newDelegateOf(impl)
}

fun newFileCollectionDependency(transitiveFilesFactory: () -> Iterable<File>) = newFileCollectionDependency(transitiveFilesFactory, transitiveFilesFactory)
fun newFileCollectionDependency(files: Iterable<File>) = newFileCollectionDependency({ files })
fun newFileCollectionDependency(vararg files: File) = newFileCollectionDependency({ files.toList() })


private interface FileCollectionDependencyInterface : FileCollectionDependency, SelfResolvingDependencyInternal

private class FileCollectionDependencyImpl(
    private val transitiveFilesFactory: () -> Iterable<File>,
    private val notTransitiveFilesFactory: () -> Iterable<File>,
    private val groupValue: String?,
    private val nameValue: String,
    private val versionValue: String?,
    private val taskDependency: TaskDependency
) {

    private val transitiveFiles: Set<File> by lazy { transitiveFilesFactory().toSet() }
    private val notTransitiveFiles: Set<File> by lazy { notTransitiveFilesFactory().toSet() }

    fun resolve(transitive: Boolean): Set<File> {
        if (transitive) {
            return transitiveFiles
        } else {
            return notTransitiveFiles
        }
    }

    fun resolve(): Set<File> {
        return resolve(true)
    }


    private val fileCollection = newFileCollection { resolve() }

    fun getFiles(): FileCollection = fileCollection


    fun getGroup(): String? = groupValue
    fun getName(): String = nameValue
    fun getVersion(): String? = versionValue


    fun contentEquals(dependency: Dependency): Boolean = dependency is FileCollectionDependency && dependency.files == getFiles()

    override fun equals(other: Any?): Boolean = other is FileCollectionDependency
        && other.group == getGroup()
        && other.name == getName()
        && other.version == getVersion()
        && other.files == getFiles()

    override fun hashCode(): Int = 1 +
        getGroup()?.hashCode().default() +
        getName().hashCode() +
        getVersion()?.hashCode().default() +
        getFiles().hashCode()


    fun copy(): Dependency = newFileCollectionDependency(
        transitiveFilesFactory = transitiveFilesFactory,
        notTransitiveFilesFactory = notTransitiveFilesFactory,
        group = groupValue,
        name = nameValue,
        version = versionValue,
        taskDependency = taskDependency
    )


    private var _reason: String? = null

    fun because(reason: String?) {
        _reason = reason
    }

    fun getReason(): String? {
        return _reason
    }


    fun getBuildDependencies(): TaskDependency = taskDependency


    fun getTargetComponentId(): ComponentIdentifier? = null

}

private val DEFAULT_NAME = FileCollectionDependency::class.java.simpleName.fromUpperCamelToLowerHyphen()
