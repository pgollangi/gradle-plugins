package name.remal.gradle_plugins.dsl.extensions

import name.remal.getCompatibleMethod
import name.remal.uncheckedCast
import org.gradle.api.artifacts.SelfResolvingDependency
import org.gradle.api.artifacts.component.ComponentIdentifier
import org.gradle.api.internal.artifacts.dependencies.SelfResolvingDependencyInternal
import org.gradle.api.internal.artifacts.dsl.dependencies.DependencyFactory.ClassPathNotation
import java.lang.reflect.Method

private val getTargetComponentIdMethod: Method by lazy {
    val declaringClass = SelfResolvingDependencyInternal::class.java
    val name = "getTargetComponentId"
    val method = declaringClass.getCompatibleMethod(ComponentIdentifier::class.java, name)
    return@lazy method
}

val SelfResolvingDependency.targetComponentId: ComponentIdentifier?
    get() {
        val method = getTargetComponentIdMethod
        if (!method.declaringClass.isInstance(this)) return null
        return method.invokeForInstance(this)
    }

fun SelfResolvingDependency.matches(classPathNotation: ClassPathNotation): Boolean {
    val targetComponentId = this.targetComponentId ?: return false
    return targetComponentId.displayName == classPathNotation.displayName
}
