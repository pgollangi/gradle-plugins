package name.remal.gradle_plugins.dsl.extensions

import org.gradle.api.Action
import java.util.function.Function as JFunction

fun <T> JFunction<T, *>.toConfigureAction() = Action<T> { apply(it) }
