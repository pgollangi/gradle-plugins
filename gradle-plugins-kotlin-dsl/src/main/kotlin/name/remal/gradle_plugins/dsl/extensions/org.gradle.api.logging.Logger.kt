package name.remal.gradle_plugins.dsl.extensions

import name.remal.arrayEquals
import name.remal.isStatic
import name.remal.plus
import name.remal.proxy.CompositeInvocationHandler
import name.remal.stream
import name.remal.toHashSet
import name.remal.uncheckedCast
import org.gradle.api.logging.Logger
import java.lang.reflect.Proxy
import java.util.stream.Stream
import kotlin.reflect.jvm.javaGetter

private val methodsToPrefixSuffix = Logger::class.java.methods.stream()
    .filter { !it.isStatic && !it.isSynthetic }
    .filter { it.returnType == Boolean::class.java && it.parameterCount == 0 }
    .filter { it.name.startsWith("is") && it.name.endsWith("Enabled") }
    .map { it.name.substring("is".length, it.name.length - "Enabled".length).toLowerCase() }
    .plus(Stream.of("log"))
    .toHashSet()

@Suppress("VariableNaming")
private interface WrappedLogger {
    val __logger: Logger
    val __messagePrefix: String
    val __messageSuffix: String
}

private val wrappedLoggerGetter = WrappedLogger::__logger.javaGetter!!.apply { isAccessible = true }
private val messagePrefixGetter = WrappedLogger::__messagePrefix.javaGetter!!.apply { isAccessible = true }
private val messageSuffixGetter = WrappedLogger::__messageSuffix.javaGetter!!.apply { isAccessible = true }

private fun Logger.wrap(prefix: String, appendPrefix: Boolean, suffix: String): Logger {
    if (prefix.isEmpty() && suffix.isEmpty()) return this

    val logger: Logger
    val fullPrefix: String
    val fullSuffix: String
    if (this is WrappedLogger) {
        logger = __logger
        if (appendPrefix) {
            fullPrefix = __messagePrefix + prefix
        } else {
            fullPrefix = prefix + __messagePrefix
        }
        fullSuffix = __messageSuffix + suffix
    } else {
        logger = this
        fullPrefix = prefix
        fullSuffix = suffix
    }

    return Proxy.newProxyInstance(
        WrappedLogger::class.java.classLoader,
        arrayOf(Logger::class.java, WrappedLogger::class.java),
        CompositeInvocationHandler()
            .prependObjectEqualsHandler()
            .prependObjectHashCodeHandler()
            .prependToStringHandler(logger.toString())
            .appendConstMethodHandler(
                { it.name == wrappedLoggerGetter.name && it.parameterTypes.arrayEquals(wrappedLoggerGetter.parameterTypes) },
                logger
            )
            .appendConstMethodHandler(
                { it.name == messagePrefixGetter.name && it.parameterTypes.arrayEquals(messagePrefixGetter.parameterTypes) },
                fullPrefix
            )
            .appendConstMethodHandler(
                { it.name == messageSuffixGetter.name && it.parameterTypes.arrayEquals(messageSuffixGetter.parameterTypes) },
                fullSuffix
            )
            .appendMethodHandler({ it.name in methodsToPrefixSuffix }) handler@{ _, method, args ->
                if (args == null) {
                    return@handler method.invoke(logger)
                }
                run {
                    val stringArgIndex = method.parameterTypes.indexOfFirst { it == String::class.java }
                    if (stringArgIndex >= 0) {
                        args[stringArgIndex] = "$fullPrefix${args[stringArgIndex]}$fullSuffix"
                        return@handler method.invoke(logger, *args)
                    }
                }
                return@handler method.invoke(logger, *args)
            }
            .appendMethodHandler({ true }) handler@{ _, method, args ->
                if (args == null) {
                    return@handler method.invoke(logger)
                } else {
                    return@handler method.invoke(logger, *args)
                }
            }
    ).uncheckedCast()
}

fun Logger.withPrefix(prefix: String, append: Boolean = false) = wrap(prefix, append, "")
fun Logger.withSuffix(suffix: String) = wrap("", false, suffix)
