package name.remal.gradle_plugins.dsl.reflective_project_plugin.info

import java.lang.reflect.Method
import kotlin.reflect.KFunction
import kotlin.reflect.jvm.javaMethod

data class ConditionMethodInfo(
    val method: Method,
    val isHidden: Boolean = false,
    val order: Int = 0,
    val description: String = ""
) : Comparable<ConditionMethodInfo> {

    constructor(
        kFunction: KFunction<*>,
        isHidden: Boolean = false,
        order: Int = 0,
        description: String = ""
    ) : this(
        method = kFunction.javaMethod ?: throw IllegalArgumentException("$kFunction can't be represented by Java method"),
        isHidden = isHidden,
        order = order,
        description = description
    )

    override fun compareTo(other: ConditionMethodInfo): Int {
        order.compareTo(other.order).let { if (it != 0) return it }
        description.compareTo(other.description).let { if (it != 0) return it }
        method.declaringClass.name.compareTo(other.method.declaringClass.name).let { if (it != 0) return it }
        method.name.compareTo(other.method.name).let { if (it != 0) return it }
        method.toString().compareTo(other.method.toString()).let { if (it != 0) return it }
        return 0
    }

}
