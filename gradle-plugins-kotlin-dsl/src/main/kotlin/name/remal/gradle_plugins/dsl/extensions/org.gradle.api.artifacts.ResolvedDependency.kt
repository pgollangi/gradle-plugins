package name.remal.gradle_plugins.dsl.extensions

import name.remal.gradle_plugins.dsl.utils.DependencyNotation
import name.remal.gradle_plugins.dsl.utils.DependencyNotationMatcher
import name.remal.nullIf
import org.gradle.api.artifacts.DependencyArtifact
import org.gradle.api.artifacts.ResolvedDependency

val ResolvedDependency.classifier: String?
    get() {
        return moduleArtifacts.singleOrNull()?.classifier
    }

val ResolvedDependency.extension: String?
    get() {
        return moduleArtifacts.singleOrNull()?.extension
    }

val ResolvedDependency.notation
    get() = DependencyNotation(
        group = moduleGroup,
        module = moduleName,
        version = moduleVersion,
        classifier = classifier,
        extension = extension.nullIf { this == DependencyArtifact.DEFAULT_TYPE }
    )


private fun ResolvedDependency.collectAllChildren(collection: MutableSet<ResolvedDependency>) {
    children.forEach {
        if (collection.add(it)) {
            it.collectAllChildren(collection)
        }
    }
}

val ResolvedDependency.allChildren: Set<ResolvedDependency> get() = mutableSetOf<ResolvedDependency>().also { collectAllChildren(it) }


fun DependencyNotationMatcher.matches(dependency: ResolvedDependency) = matches(dependency.notation)
fun DependencyNotationMatcher.notMatches(dependency: ResolvedDependency) = !matches(dependency)
