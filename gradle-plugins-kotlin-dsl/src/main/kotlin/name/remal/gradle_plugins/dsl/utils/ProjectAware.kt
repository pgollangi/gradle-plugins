package name.remal.gradle_plugins.dsl.utils

import org.gradle.api.Project

interface ProjectAware {

    var project: Project

}
