package name.remal.gradle_plugins.dsl.utils

import name.remal.retry
import java.io.IOException

inline fun <R> retryIO(action: () -> R): R = retry(5, IOException::class.java, 2500, action)
