package name.remal.gradle_plugins.dsl.extensions

import name.remal.concurrentMapOf
import org.gradle.api.artifacts.Configuration
import org.gradle.api.artifacts.ConfigurationContainer
import org.gradle.api.artifacts.Dependency

fun ConfigurationContainer.createDependencyTransformConfiguration(
    configuration: Configuration,
    newConfigurationName: String,
    isRecursive: Boolean = true,
    transformer: (dependency: Dependency) -> Dependency
): Configuration {
    val resultConf = create(newConfigurationName)

    val dependenciesMapping = concurrentMapOf<Dependency, Dependency>()
    val dependenciesCollection = if (isRecursive) configuration.allDependencies else configuration.dependencies
    dependenciesCollection.whenObjectAdded { dependency ->
        val transformedDependency = transformer(dependency)
        dependenciesMapping[dependency] = transformedDependency
        resultConf.dependencies.add(transformedDependency)
    }
    dependenciesCollection.whenObjectRemoved { dependency ->
        dependenciesMapping[dependency]?.let { resultConf.dependencies.remove(it) }
    }

    return resultConf
}
